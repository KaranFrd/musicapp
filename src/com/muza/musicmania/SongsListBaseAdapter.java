package com.muza.musicmania;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.muza.music.R;

public class SongsListBaseAdapter extends BaseAdapter 
		 {

	private LayoutInflater inflater;
	Context context;

	List<Songdetail> rowItems;
	public SongsListBaseAdapter(Context context,List<Songdetail> rowItems)
	{
		inflater = LayoutInflater.from(context);
		 this.context = context;
		 this.rowItems = rowItems;
	}


	private class ViewHolder {
        TextView Sex;
        TextView Pin;
        TextView Name;
    }
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
 
        LayoutInflater mInflater = (LayoutInflater)
            context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.main, null);
            holder = new ViewHolder();
            holder.Name = (TextView) convertView.findViewById(R.id.name);
            holder.Pin = (TextView) convertView.findViewById(R.id.detail);

//            holder.Name = (TextView) convertView.findViewById(R.id.name);
//            holder.Pin = (TextView) convertView.findViewById(R.id.cost);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
 
        Songdetail rowItem = (Songdetail) getItem(position);
        holder.Name.setText(rowItem.SongName);
        holder.Pin.setText(rowItem.Songsize);

//        holder.Sex.setText(rowItem.Sex);
//       holder.Pin.setText(rowItem.BBmpin);
 
        return convertView;
    }
 
    @Override
    public int getCount() {
        return rowItems.size();
    }
 
    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }

	public void clear() {
		// TODO Auto-generated method stub
		
	}


	
	
}

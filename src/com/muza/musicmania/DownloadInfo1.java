package com.muza.musicmania;

import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;


public class DownloadInfo1 {
  private final static String TAG = DownloadInfo1.class.getSimpleName();
  public enum DownloadState {
    NOT_STARTED,
    QUEUED,
    DOWNLOADING,
    COMPLETE
  }
  private volatile DownloadState mDownloadState = DownloadState.NOT_STARTED;
  private final String mFilename;
  private volatile Integer mProgress;
  public final Integer mFileSize;
  private volatile ProgressBar mProgressBar;
  
  private volatile TextView txtdownload;

  	public boolean isPause;
  	public int   mFileSizecompleted =0;

  public DownloadInfo1(String filename, Integer size) {
    mFilename = filename;
    mProgress = 0;
    mFileSize = size;
    mProgressBar = null;

  }
  
  public ProgressBar getProgressBar() {
    return mProgressBar;
  }
  public void settextview(TextView txtview) {
    txtdownload = txtview;
  }
  
  public TextView getTextview() {
	    return txtdownload;
	  }
	  public void setProgressBar(ProgressBar progressBar) {
	    Log.d(TAG, "setProgressBar " + mFilename + " to " + progressBar);
	    mProgressBar = progressBar;
	  }
	  
  
  public void setDownloadState(DownloadState state) {
    mDownloadState = state;
  }
  public DownloadState getDownloadState() {
    return mDownloadState;
  }
  
  public Integer getProgress() {
    return mProgress;
  }

  public void setProgress(Integer progress) {
    this.mProgress = progress;
    this.mFileSizecompleted=progress;
  }

  public Integer getFileSize() {
    return mFileSize;
  }

  public String getFilename() {
    return mFilename;
  }
  public int getdownloadsize() {
	    return mFileSizecompleted;
	  }
}

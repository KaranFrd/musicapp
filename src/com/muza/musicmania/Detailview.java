 
package com.muza.musicmania;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.muza.music.R;
import com.muza.musicmania.DownloadInfo1.DownloadState;

/**
 * @author janakz
 * 
 */
public class Detailview extends Activity implements OnTouchListener,
		MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
		MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener,
		AdListener {

	private Utilities utilsn;
	ProgressDialog progressDialog;

	private SeekBar progressSongDuration;
	private ImageView imgAlbumImage;
	private ImageView imgDownLoad;
	private ImageView imgShare;

	private ImageView btnPlayPause;
	private TextView txtStartTime;
	private TextView txtEndTime;
	private TextView txtSongName;
	int n = 10000;

	Boolean isCancelled = false;

	private int mediaFileLengthInMilliseconds;

	private int selectedSongPosition;
	private Runnable notificationRunnable;
	private final Handler handler = new Handler();

	private String albumName;
	private String songName;
	private String imageUrl;
	public int songIndex;
	Random generator = new Random();

	// For DwonaLoad Notification.
	Notification notification;
	NotificationManager notificationManager;
	CharSequence contentText;
	Context context;

	// Display Notification,
	private long time;
	private int icon;
	CharSequence tickerText;
	PendingIntent contentIntent;
	CharSequence contentTitle;
	int HELLO_ID = 1;
	String fname;
	PlaySongAsy play;
	private String TAG = getClass().getSimpleName();
	private MediaPlayer mp = null;
	boolean isPlaying = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// admob full adpause.pngpause.pngsec.xmlsec.xmlsec.xmlsec.xmlsec.xml

		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.utilsn = new Utilities();
		setContentView(R.layout.sec);
		intializaView();

		// imageUrl = getIntent().getExtras().getString("imageUrl");
		songName = getIntent().getExtras().getString("songName");
		songIndex = getIntent().getExtras().getInt("songIndex");
		txtSongName.setText(songName);

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setImageonImageView(imgAlbumImage, imageUrl);
				txtSongName.setText(songName);

			}
		});

		context=getApplicationContext();
		/*
		 * play=(PlaySongAsy) new
		 * PlaySongAsy(DisplaySongActivity.songList.get(songIndex)
		 * .getAlbumSongsLink().replace(" ", "%20").trim()).execute();
		 */

	}



	class PlaySongAsy extends AsyncTask<String, Void, Boolean> {

		String baseURL;

		public PlaySongAsy(String baseURL) {
			this.baseURL = baseURL;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Detailview.this,
					"    Buffering...", "please wait..", false);
			progressDialog.setCancelable(false);

		}

		@Override
		protected Boolean doInBackground(String... urls) {

			// play(baseURL);
			new Thread() {
				@Override
				public void run() {
					play(baseURL);
				}
			}.start();
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			// progressDialog.dismiss();
		}
	}

	private void play(String baseURL) {
		Uri myUri = Uri.parse(baseURL);
		try {
			if (mp == null) {
				this.mp = new MediaPlayer();
			} else {
				mp.stop();
				mp.reset();
			}
			mp.setDataSource(this, myUri); // Go to Initialized state

			mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.setOnPreparedListener(this);
			mp.setOnBufferingUpdateListener(this);
			mp.setOnErrorListener(this);

			mp.prepareAsync();

			// mp.setVolume(5.F, 5.F);

			Log.d(TAG, "LoadClip Done");
		} catch (Throwable t) {
			Log.d(TAG, t.toString());
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// mp.stop();

		isCancelled = true;

		// PlaySongActivity.this.finish();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		Log.d(TAG, "Stream is prepared");
		mp.start();
		isPlaying = true;
		btnPlayPause.setImageResource(R.drawable.pause);
	}

	private void pause() {
		mp.pause();
		isPlaying = false;
	}

	private void stop() {
		mp.stop();
		isPlaying = false;
		btnPlayPause.setImageResource(R.drawable.pause);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mp != null) {
			stop();
			mp.setOnBufferingUpdateListener(null);

		}

	}

	public void onCompletion(MediaPlayer mp) {
		stop();
	}

	public boolean onError(MediaPlayer mp, int what, int extra) {
		StringBuilder sb = new StringBuilder();
		sb.append("Media Player Error: ");
		switch (what) {
		case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
			sb.append("Not Valid for Progressive Playback");
			break;
		case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
			sb.append("Server Died");
			break;
		case MediaPlayer.MEDIA_ERROR_UNKNOWN:
			sb.append("Unknown");
			break;
		default:
			sb.append(" Non standard (");
			sb.append(what);
			sb.append(")");
		}
		sb.append(" (" + what + ") ");
		sb.append(extra);
		Log.e(TAG, sb.toString());
		return true;
	}

	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		if ((mp != null) && (mp.isPlaying())) {
			progressDialog.dismiss();

			primarySeekBarProgressUpdater();
		}
		if (isCancelled) {

		}
		Log.d(TAG, "PlayerService onBufferingUpdate : " + percent + "%");
		// bufPercent=percent;
	}

	private void intializaView() {
		// TODO Auto-generated method stub

		progressSongDuration = (SeekBar) findViewById(R.id.progbarSong);
		progressSongDuration.setMax(99); // It means 100% .0-99
		progressSongDuration.setOnTouchListener(this);

		// imgAlbumImage = (ImageView) findViewById(R.id.imgAlbum);

		imgDownLoad = (ImageView) findViewById(R.id.imgDownload);
		imgDownLoad.setOnClickListener(onClickListener);

		imgShare = (ImageView) findViewById(R.id.imgShare);
		imgShare.setOnClickListener(onClickListener);

		btnPlayPause = (ImageView) findViewById(R.id.btnPlayPause);
		btnPlayPause.setOnClickListener(onClickListener);

		txtStartTime = (TextView) findViewById(R.id.txtStartTime);
		txtEndTime = (TextView) findViewById(R.id.txtEndTime);

		txtSongName = (TextView) findViewById(R.id.txtSongName);
		txtSongName.setTextColor(Color.WHITE);
		txtSongName.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);

	}

	private void setImageonImageView(ImageView imgAlbumImage, String imageUrl) {
		try {
			// imageLoader.DisplayImage(imageUrl, imgAlbumImage);

		} catch (Exception ex) {
			System.out.println("Error in load Image-->" + ex.toString());
		}
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			if (v.getId() == R.id.btnPlayPause) {
				System.out.println("Button play Click");
				if (mp != null) {
					System.out.println("Button play Click");
					if (isPlaying) {
						isPlaying = false;
						pause();
						btnPlayPause.setImageResource(R.drawable.playicon);
					} else {
						isPlaying = true;
						mp.start();
						btnPlayPause.setImageResource(R.drawable.pause);
					}

				} else {
					play = (PlaySongAsy) new PlaySongAsy(Cons.playerList
							.get(songIndex).getLink().replace(" ", "%20")
							.trim()).execute();
				}
			} else if (v.getId() == R.id.imgDownload) {
				System.out.println("Button Download Click");
				startDownload();

			} else if (v.getId() == R.id.imgShare) {
				System.out.println("Button share Click");
				// TODO Auto-generated method stub
				{
					Intent intent = new Intent(Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.putExtra(
							Intent.EXTRA_TEXT,
							"Songs "
									+ Cons.playerList.get(songIndex).getName()

									+ "\n"
									+ "Get All Type of New Songs \n"
									+ "Download this App Click to Below link "
									+ "http://play.google.com/store/apps/details?id=com.freemusicappz.allinonemusic");
					startActivity(Intent.createChooser(intent, "Share via"));
				}

			}
			songName = Cons.playerList.get(songIndex).getName();

			txtSongName.setText(songName);
		}

	};

	public boolean isInternetAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null)
			return (cm.getActiveNetworkInfo().isConnected() && cm
					.getActiveNetworkInfo().isAvailable());
		else {
			return false;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		if (v.getId() == R.id.progbarSong) {
			/**
			 * Seekbar onTouch event handler. Method which seeks MediaPlayer to
			 * seekBar primary progress position
			 */
			if (mp != null) {
				if (mp.isPlaying()) {
					SeekBar sb = (SeekBar) v;
					int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100)
							* sb.getProgress();
					mp.seekTo(playPositionInMillisecconds);
				}
			}
		}
		return false;
	}

	private void primarySeekBarProgressUpdater() {

		if (mp != null) {
			long l1 = mp.getDuration();
			long l2 = mp.getCurrentPosition();
			txtEndTime.setText(utilsn.milliSecondsToTimer(l1));
			txtStartTime.setText(utilsn.milliSecondsToTimer(l2));

			mediaFileLengthInMilliseconds = mp.getDuration();
			// SeekBar.setProgress((int)(100.0F * (this.mps.getCurrentPosition()
			// /
			// this.mediaFileLengthInMilliseconds)));
			int temp123 = mp.getCurrentPosition();

			float temp11 = ((temp123 * 100.0F) / mediaFileLengthInMilliseconds);

			int temp = (int) (temp11);
			progressSongDuration.setProgress(temp);
			if (mp.isPlaying()) {
				Runnable local12 = new Runnable() {
					public void run() {
						primarySeekBarProgressUpdater();
					}
				};
				this.handler.postDelayed(local12, 1000L);
			}
		}
	}

	private void startDownload() {

		new AlertDialog.Builder(this)
				.setIcon(R.drawable.download)
				.setTitle("Download Mp3 Song")
				.setMessage("Are you sure you want to Download this Songs?")
				.setPositiveButton("Download",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							
										final DownloadFileAsync localDownloadFileAsync = new DownloadFileAsync(getApplicationContext());
										final String[] arrayOfString = new String[3];
										arrayOfString[0] = Cons.playerList.get(
												songIndex).getLink();
										arrayOfString[1] = Cons.playerList.get(
												songIndex).getName();
										arrayOfString[2]="True";
									
									localDownloadFileAsync.execute(arrayOfString);	
																		
								
//								Intent i = new Intent(Detailview.this,
//								DownloadsList.class);
//								
//								i.putExtra("url",Cons.playerList.get(songIndex).getLink());
//								i.putExtra("name",Cons.playerList.get(songIndex).getName());
//								i.putExtra("album",true);
//								i.putExtra("isfrom",true);
//
//								startActivity(i);
								
								
							
								
							
								
								
//								String Temp =Cons.playerList.get(
//										songIndex).getLink();
//								
//								String decoded = Temp.replace(" ", "%20");
//
//								
//						        BufferedOutputStream bout=null;
//						        boolean isAlbum=true;
//						    	File localFile1; 
//								
//								if(isAlbum)
//								{
//									 localFile1 = new File("/mnt/sdcard/Songs Downloaded");
//								}
//								else
//								{
//									 localFile1 = new File("/mnt/sdcard/Ringtones Downloaded");	
//								}
								
								
//								Intent i = new Intent(Detailview.this,
//										DownloadsList.class);
//								i.putExtra(App.KEY_URL, Temp);
//								i.putExtra(App.KEY_FILE_NAME, Cons.playerList.get(
//										songIndex).getName());
//								i.putExtra(App.KEY_FILE_DIR, localFile1.toString());
//								startActivity(i);
								
								
//								DownloadsList dow=new DownloadsList(); 
//								dow.startDownload(Temp, Cons.playerList.get(
//										songIndex).getName(), localFile1.toString(), 8);
								
								
//								final DownloadInfo info = App.newDownload(Temp, Cons.playerList.get(
//										songIndex).getName(), localFile1.toString(), 8);
//								info.downloader = new FileDownloader(info, this);
//								runOnUiThread(new Runnable() {
//									@Override
//									public void run() {
//										info.downloader.execute();
//									}
//								});		
										
										
										
						
										

								

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).show();

	}
	private void finish(Intent intent, Bundle extras, File file, int threads) 
	{
//		extras.putString(App.KEY_FILE_NAME, file.getName());
//		extras.putString(App.KEY_FILE_DIR, file.getParent() + File.separator);
//		extras.putInt(App.KEY_THREADS, threads);
//		intent.putExtras(extras);
//		setResult(RESULT_OK, intent);
//		startActivity(intent);
	}
	public void downloadNotification() {
		this.notificationManager = ((NotificationManager) getSystemService("notification"));
		this.icon = 2130837512;
		this.tickerText = "Downloading...";
		this.time = System.currentTimeMillis();
		this.notification = new Notification(R.drawable.ic_launcher,
				this.tickerText, this.time);
		Notification localNotification = this.notification;
		localNotification.flags = (0x18 | localNotification.flags);
		this.context = getApplicationContext();
		this.contentTitle = ("Downloading " + songName);
		this.contentText = "0% complete";
		Uri localUri = Uri.parse("file:///sdcard/Mp3 Downloaded Songs/"
				+ songName);
		Intent localIntent = new Intent("android.intent.action.VIEW");
		localIntent.setDataAndType(localUri, "audio/mp3");
		this.contentIntent = PendingIntent.getActivity(this.context, 0,
				localIntent, 0);
		this.notification.setLatestEventInfo(this.context, this.contentTitle,
				this.contentText, this.contentIntent);
		this.notificationManager.notify(this.HELLO_ID, this.notification);
	}

	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad ad) {
		// TODO Auto-generated method stub

	}

}

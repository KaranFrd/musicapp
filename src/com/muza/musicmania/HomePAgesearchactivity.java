package com.muza.musicmania;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.muza.music.R;

@SuppressLint("NewApi")
public class HomePAgesearchactivity extends Activity{
	// TabSpec Names
	private static final String SERVER3 = "SERVER3";
	EditText editText;
	Button btngo;
	ImageView imgMusic, imgMusic1;

	public Boolean isalbum;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newon);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Fixed
																			// Portrait
																			// orientation
		// final TabHost tabHost = getTabHost();

		// Inbox Tab
		// TabSpec inboxSpec = tabHost.newTabSpec(SERVER1);

		Intent intent1 = getIntent();

		isalbum = intent1.getBooleanExtra("isalbum", true);

		

		imgMusic = (ImageView) findViewById(R.id.imgMusic);
		imgMusic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(HomePAgesearchactivity.this,
						DownloadedSongList.class);
				i.putExtra("isalbum",true);

				startActivity(i);
			}
		});

		imgMusic1 = (ImageView) findViewById(R.id.imgMusic1);

		imgMusic1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//
				Intent playSongIntent = new Intent(HomePAgesearchactivity.this,
						MainActivity.class);
				// playSongIntent.putExtra("imageUrl", imageUrl);
				// playSongIntent.putExtra("albumName", albumName);
				// playSongIntent.putExtra("songIndex", childPosition);
				// playSongIntent.putExtra("songName",
				// albumSongList.getAlbumSongsName());

				startActivity(playSongIntent);
				
//				Intent i = new Intent(HomePAgesearchactivity.this,
//						DownloadsList.class);
//						i.putExtra("isfrom",false);
//						startActivity(i);
					
			}
		});
		editText = (EditText) findViewById(R.id.editText1);
		btngo = (Button) findViewById(R.id.btn_search);

		btngo.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub

				Cons.songname = editText.getText().toString();

				if (Cons.songname != null && !Cons.songname.isEmpty()) {
					Cons.listAlbums.clear();
					Cons.listAlbumsserver2.clear();
					Cons.listAlbumsserver3.clear();
					Cons.listAlbumsserver4.clear();
					Cons.listAlbumsserver5.clear();
					Cons.playerList.clear();

					// Log.i("***Selected Tab",
					// "Im currently in tab with index::" +
					// tabHost.getCurrentTab());

					Intent i = new Intent(HomePAgesearchactivity.this,
							AndroidTabAndListView.class);
					startActivity(i);
					InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(getCurrentFocus()
							.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);

				} else {
					AlertDialog localAlertDialog = new AlertDialog.Builder(
							HomePAgesearchactivity.this).create();
					localAlertDialog.setTitle("Input Text!");
					localAlertDialog.setMessage("Please Enter Song Name.");
					localAlertDialog.setButton("Ok",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									//
									arg0.dismiss();
								}
							});

					localAlertDialog.show();
				}

			}
		});

		//

		// Tab Icon
		OnTouchListener focusHandler = new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				// TODO Auto-generated method stub
				view.requestFocusFromTouch();
				return false;
			}
		};
		// editText.setOnTouchListener(focusHandler); //For each EditText with
		// this issue

	}

	// public void onBackPressed() {
	// // TODO Auto-generated method stub
	// AlertDialog.Builder builder = new AlertDialog.Builder(
	// HomePAgesearchactivity.this);
	// builder.setIcon(R.drawable.sticker);
	// builder.setTitle("Stickers & Emotions");
	// builder.setMessage("All Type of Stickers free Download!")
	//
	// .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	// AdRequest adRequest = new AdRequest();
	// adRequest.addTestDevice("862107FD2563D65A8955698A1D0E059A");
	// interstitial.loadAd(adRequest);
	// timer.cancel();
	// finish();
	// }
	// }).setNegativeButton("Install", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setData(Uri
	// .parse("http://play.google.com/store/apps/details?id=com.dailyappshub.stickers"));
	// startActivity(intent);
	// dialog.cancel();
	// }
	// }).setNeutralButton("Rate Us!", new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	//
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setData(Uri
	// .parse("http://play.google.com/store/apps/details?id=com.dailyappshub.musicdownloader"));
	// startActivity(intent);
	// dialog.cancel();
	// }
	// }).show();
	// }

	

}
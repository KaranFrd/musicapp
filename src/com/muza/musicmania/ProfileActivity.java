package com.muza.musicmania;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.muza.music.R;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;

@SuppressLint("NewApi")
public class ProfileActivity extends Activity implements AdListener {

	public ProgressDialog dialog;
	AdView adview;
	public ArrayAdapter<String> adapter;
	public Context cont;
	ListView songsListView;
	EditText txt;
	Button loadmore;
	int page = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homeacivity);
		// AddAsyncTask task = new AddAsyncTask();
		// task.execute(new String[] { "" });
		// cont=getApplicationContext();

		adview = (AdView) findViewById(R.id.adView);
		adview.setVisibility(View.GONE);
		songsListView = (ListView) findViewById(R.id.ListView);
		View footerView = ((LayoutInflater) getApplicationContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.footer, null, false);
		songsListView.addFooterView(footerView);
		loadmore = (Button) findViewById(R.id.loadMore);
		loadmore.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				page++;

				AddAsyncTask task = new AddAsyncTask();
				task.execute("");

			}
		});
		// btngo = (Button) findViewById(R.id.btnGo);
		// txt = (EditText) findViewById(R.id.editText1);
		// btngo.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// Cons.listAlbumsserver3.clear();
		//
		// AddAsyncTask task = new AddAsyncTask();
		// task.execute(new String[] { "" });
		//
		// InputMethodManager inputManager = (InputMethodManager)
		// getApplicationContext()
		// .getSystemService(Context.INPUT_METHOD_SERVICE);
		// inputManager.hideSoftInputFromWindow(getCurrentFocus()
		// .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		//
		// }
		// });
		songsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Cons.playerList = Cons.listAlbumsserver3;
				Intent playSongIntent = new Intent(ProfileActivity.this,
						Detailview.class);
				playSongIntent.putExtra("songIndex", position);
				playSongIntent.putExtra("songName",
						Cons.listAlbumsserver3.get(position).SongName);

				startActivity(playSongIntent);

			}
		});

		AddAsyncTask task = new AddAsyncTask();
		task.execute("");
		// ((EditText) findViewById(R.id.editText1))
		// .setOnEditorActionListener(new EditText.OnEditorActionListener() {
		// @Override
		// public boolean onEditorAction(TextView v, int actionId,
		// KeyEvent event) {
		// Cons.listAlbumsserver3.clear();
		// AddAsyncTask task = new AddAsyncTask();
		// task.execute(new String[] { "" });
		// InputMethodManager inputManager = (InputMethodManager)
		// getApplicationContext()
		// .getSystemService(Context.INPUT_METHOD_SERVICE);
		// inputManager.hideSoftInputFromWindow(getCurrentFocus()
		// .getWindowToken(),
		// InputMethodManager.HIDE_NOT_ALWAYS);
		// return true; // consume.
		//
		// }
		// });

	}



	private static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	private static String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width - 1) + ".";
		else
			return s;
	}

	public class AddAsyncTask extends
			AsyncTask<String, Void, ArrayList<Songdetail>>

	{
		private ArrayAdapter arrayAdapter;

		ArrayList<Songdetail> Songlist = null;
		String currentvcall;
		Songdetail songdetail;

		@Override
		protected ArrayList<Songdetail> doInBackground(String... urls) {
			// dialog = ProgressDialog.show(MainActivity.this, "",
			// "Searching for Songs�", true);
			currentvcall = urls[0];

			Document doc;
			try {
				Elements localElements1 = Jsoup
						.connect(
								"http://www.dilandau.eu/scaricare_canzoni-mp3/"
										+ Cons.songname.trim().replace(" ",
												"%20") + "/" + page + ".html").timeout(100000)
						.userAgent(
								"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36")
						.get().select("div.mbottom20.sbox");
				Elements localElements2 = localElements1.select("td.name");
				Elements localElements3 = localElements1.select("td.size");
				Elements localElements4 = localElements1
						.select("a.mp3_6483.button.button-s.button-1.download-track");
				Elements localElements5 = localElements2.select("H4");
				Log.e("lins2", localElements4.toString());
				for (int i = 0; i < localElements5.size(); i++) {
					Log.e("lins", localElements5.get(i).text());
					Log.e("lins", localElements3.get(i).text());
					Log.e("lins",
							localElements4.get(i).select("a[href]")
									.attr("href")
									+ localElements4.get(i).select("a[href]")
											.attr("url"));
					songdetail = new Songdetail();
					songdetail.SongLink = localElements4.get(i)
							.select("a[href]").attr("href")
							+ localElements4.get(i).select("a[href]")
									.attr("url");
					songdetail.SongName = localElements5.get(i).text() + ".mp3";
					songdetail.Songsize = localElements3.get(i).text();
					Cons.listAlbumsserver3.add(songdetail);
				}
			} catch (Exception e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Cons.listAlbumsserver3;
		}

		@Override
		protected void onPostExecute(ArrayList<Songdetail> xml) {

			dialog.dismiss();

			if (Cons.listAlbumsserver3.size() == 0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ProfileActivity.this);
				builder.setTitle("Please Try Again Later!").setPositiveButton(
						"OK",
						new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

							}
						});
				builder.create().show();
			} else {

				String[] songsArray = new String[Cons.listAlbumsserver3.size()];

				// Fill the songs array by using a for loop
				for (int i = 0; i < Cons.listAlbumsserver3.size() - 1; i++) {
					songdetail = Cons.listAlbumsserver3.get(i);
					songsArray[i] = songdetail.SongName;
				}

				int currentPosition = songsListView.getFirstVisiblePosition();
				 
				songsListView.setAdapter(new SongsListBaseAdapter(getApplicationContext(),
						Cons.listAlbumsserver3));
				songsListView.setSelectionFromTop(currentPosition + 1, 0);

			}

		}

		protected void onPreExecute() {
			super.onPreExecute();
			dialog = ProgressDialog.show(ProfileActivity.this, "",
					"Searching for Songs�", true);
		}
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad ad) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		super.onResume();

		// Update your UI here.
	}

	public static void updatedata() {
		if (Cons.songname != null && !Cons.songname.isEmpty()) {

		}

	}

}

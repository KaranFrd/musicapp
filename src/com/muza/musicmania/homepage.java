package com.muza.musicmania;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.audiofx.BassBoost.Settings;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.InterstitialAd;
import com.muza.music.R;

public class homepage extends Activity implements AdListener {
	// TabSpec Names
	EditText editText;
	Button btnsearch, btntone, btnalbum, btncutter, btndownloadsong;
			
	ImageView imgMusic;
	private InterstitialAd interstitial;
	Timer timer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homeacitvity);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // Fixed

		// Portrait
		//a152c40faf59148
		interstitial = new InterstitialAd(this, "a152c40faf59148");
		// Create ad request
		// AdRequest adRequest = new AdRequest();
		// adRequest.addTestDevice("862107FD2563D65A8955698A1D0E059A");
		// Begin loading your interstitial
		// interstitial.loadAd(adRequest);

		// Set Ad Listener to use the callbacks below
		interstitial.setAdListener(this);
		initFullScreenAdTimer();
		btnsearch = (Button) findViewById(R.id.btnSearch);
		btnsearch.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(homepage.this,
						HomePAgesearchactivity.class);
				startActivity(i);

			}
		});

		btnalbum = (Button) findViewById(R.id.btnalbum);
		btnalbum.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(homepage.this, DisplaySongActivity.class);
				i.putExtra("isalbum", true);
				startActivity(i);

			}
		});

		btntone = (Button) findViewById(R.id.btnTone);
		btntone.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(homepage.this, DisplaySongActivity.class);
				i.putExtra("isalbum", false);
				startActivity(i);

			}
		});
		//
		btncutter = (Button) findViewById(R.id.btncutter);
		btncutter.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(homepage.this,
						RingdroidSelectActivity.class);
				startActivity(i);

			}
		});

		btndownloadsong = (Button) findViewById(R.id.btndownloadedSongs);
		btndownloadsong.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// TODO Auto-generated method stub

				Intent i = new Intent(homepage.this, Downloadsongcat.class);
				startActivity(i);

			}
		});

//		btnDownloading = (Button) findViewById(R.id.btndownloadingprocess);
//		btnDownloading.setOnClickListener(new OnClickListener() {
//
//			public void onClick(View v) {
//
//				// TODO Auto-generated method stub
//
//				Intent playSongIntent = new Intent(homepage.this,
//						MainActivity.class);
//				// playSongIntent.putExtra("imageUrl", imageUrl);
//				// playSongIntent.putExtra("albumName", albumName);
//				// playSongIntent.putExtra("songIndex", childPosition);
//				// playSongIntent.putExtra("songName",
//				// albumSongList.getAlbumSongsName());
//
//				startActivity(playSongIntent);
//
//			}
//		});
		
		
		final SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		
		
		if(prefs.contains("firstTime"))
		{
			
		 int avedValue = prefs.getInt("firstTime", 0);
		 
		 avedValue++;
		 
		 SharedPreferences.Editor editor = prefs.edit();
         editor.putInt("firstTime", avedValue);
         editor.commit();
         
         
         
         if(avedValue>=4)
         {
        	 
     		if(prefs.contains("rateit"))
     		{
     			
     		}
     		else
     		{
        	 AlertDialog.Builder builder = new AlertDialog.Builder(homepage.this);
     		builder.setTitle("   Rate me in market");
     		builder.setMessage("Dear user, if you like our app, please give us 5 stars. Your sustained support is the source of our improvement.")
     				.setPositiveButton("Cancel",
     						new DialogInterface.OnClickListener() {
     							public void onClick(DialogInterface dialog, int id) 
     							{
     								dialog.dismiss();
     							}
     						})
     				.setNeutralButton("Vote",
     						new DialogInterface.OnClickListener() {
     							public void onClick(DialogInterface dialog, int id) {

     								Intent intent = new Intent(Intent.ACTION_VIEW);
     								intent.setData(Uri
     										.parse("http://play.google.com/store/apps/details?id=com.freemusicappz.allinonemusic"));
     								startActivity(intent);
     								dialog.cancel();
     								

     								SharedPreferences.Editor editor = prefs.edit();
     					            editor.putBoolean("rateit", true);
     					            editor.commit();
     								
     							}
     						}).show();
     		}
         }
		}
		else
		{

			SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("firstTime", 1);
            editor.commit();
			
			
		}
		 

	}

	@Override
	public void onBackPressed() {
		
		
		AdRequest adRequest = new AdRequest();
		// adRequest.addTestDevice("862107FD2563D65A8955698A1D0E059A");
		interstitial.loadAd(adRequest);
		finish();
		timer.cancel();
	
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad ad) {
		// TODO Auto-generated method stub
		if (ad == interstitial) {
			interstitial.show();
		}
	}

	private void initFullScreenAdTimer() {
		timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				
				
				
				timerMethod();
			}
		},  100000, (3 * 60 * 1000));
	}

	private void timerMethod() {
		this.runOnUiThread(new Runnable() {
			public void run() {
				// Create the interstitial

				AdRequest adRequest = new AdRequest();
 
				// Begin loading your interstitial
				interstitial.loadAd(adRequest);

			}
		});

	}

}
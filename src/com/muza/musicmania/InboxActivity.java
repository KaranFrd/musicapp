package com.muza.musicmania;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.muza.music.R;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;

public class InboxActivity extends Activity implements AdListener {

	public ProgressDialog dialog;

	public ArrayAdapter<String> adapter;
	public Context cont;
	ListView songsListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homeacivity);
		// AddAsyncTask task = new AddAsyncTask();
		// task.execute(new String[] { "" });
		// cont=getApplicationContext();

		songsListView = (ListView) findViewById(R.id.ListView);

		songsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Cons.playerList = Cons.listAlbumsserver2;
				Intent playSongIntent = new Intent(InboxActivity.this,
						Detailview.class);
				playSongIntent.putExtra("songIndex", position);
				playSongIntent.putExtra("songName",
						Cons.listAlbumsserver2.get(position).SongName);

				startActivity(playSongIntent);

			}
		});

		AddAsyncTask task = new AddAsyncTask(InboxActivity.this);
		task.execute("");
	}

	

	private static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	private static String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width - 1) + ".";
		else
			return s;
	}

	public class AddAsyncTask extends
			AsyncTask<String, Void, ArrayList<Songdetail>>

	{
		private ArrayAdapter arrayAdapter;

		ArrayList<Songdetail> Songlist = null;
		String currentvcall;
		Songdetail songdetail;
		Context con;

		public AddAsyncTask(Context con) {
			this.con = con;
		}

		@Override
		protected ArrayList<Songdetail> doInBackground(String... urls) {
			// dialog = ProgressDialog.show(MainActivity.this, "",
			// "Searching for Songs�", true);
			currentvcall = urls[0];

			Document doc;
			try {
				doc = Jsoup
						.parse(Jsoup
								.connect(
										"http://mp3skull.com/mp3/"
												+ Cons.songname + ".html")
								.timeout(100000)
								.userAgent(
										"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 1.2.30703)")
								.get().html());
				Elements links = doc.select("div#song_html");
				for (Element src : links) {
					if (src.id().equals("song_html")) {
						Elements linkstemp = src.select("a[href]");
						String temp11 = linkstemp.attr("abs:href");
						String e = src.select("b").get(0).text();
						String e1 = src.select("div[class=left]").get(0).text();

						songdetail = new Songdetail();
						songdetail.SongLink = temp11;
						songdetail.SongName = e + ".mp3";
						songdetail.Songsize = e1;
						Cons.listAlbumsserver2.add(songdetail);
					}

					else
						print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Cons.listAlbumsserver2;
		}

		@Override
		protected void onPostExecute(ArrayList<Songdetail> xml) {

			try {
				dialog.dismiss();

				if (Cons.listAlbumsserver2.size() == 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							this.con);
					builder.setTitle("Please Try Again Later!")
							.setPositiveButton(
									"OK",
									new android.content.DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub

										}
									});
					builder.create().show();
				} else {

					String[] songsArray = new String[Cons.listAlbumsserver2.size()];

					// Fill the songs array by using a for loop
					for (int i = 0; i < Cons.listAlbumsserver2.size() - 1; i++) {
						songdetail = Cons.listAlbumsserver2.get(i);
						songsArray[i] = songdetail.SongName;
					}

					songsListView.setAdapter(new SongsListBaseAdapter(this.con,
							Cons.listAlbumsserver2));

				}

			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}

		protected void onPreExecute() {

			try {
				super.onPreExecute();
				dialog = ProgressDialog.show(this.con, "",
						"Searching for Songs�", true);

			} catch (Exception ex) {
				ex.printStackTrace();

			}
		}
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onReceiveAd(Ad ad) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	public void updtae() {

	}
}

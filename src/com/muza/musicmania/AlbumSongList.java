/**
 * 
 */
package com.muza.musicmania;

/**
 * @author janakz
 *
 */
public class AlbumSongList 
{
	private String albumSongsName;
	private String albumSongsLink;
	
	/**
	 * @return the albumSongsName
	 */
	public String getAlbumSongsName() {
		return albumSongsName;
	}
	/**
	 * @param albumSongsName the albumSongsName to set
	 */
	public void setAlbumSongsName(String albumSongsName) {
		this.albumSongsName = albumSongsName;
	}
	/**
	 * @return the albumSongsLink
	 */
	public String getAlbumSongsLink() {
		return albumSongsLink;
	}
	/**
	 * @param albumSongsLink the albumSongsLink to set
	 */
	public void setAlbumSongsLink(String albumSongsLink) {
		this.albumSongsLink = albumSongsLink;
	}
	
}

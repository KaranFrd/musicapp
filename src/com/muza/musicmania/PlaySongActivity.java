/**
 * 
 */
package com.muza.musicmania;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.muza.allringtones.lazy.ImageLoader;
import com.muza.music.R;
import com.muza.musicmania.DownloadInfo1.DownloadState;

/**
 * @author janakz
 * 
 */
public class PlaySongActivity extends Activity implements OnTouchListener,
		MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
		MediaPlayer.OnErrorListener, MediaPlayer.OnBufferingUpdateListener {

	private Utilities utilsn;
	ProgressDialog progressDialog;
	private ImageView btnBack;
	private ImageView btnMore;
	private SeekBar progressSongDuration;
	private ImageView imgAlbumImage;
	private ImageView imgDownLoad;
	private ImageView imgShare;
	private ImageView btnNext;
	private ImageView btnPrevious;
	private ImageView btnPlayPause;
	private TextView txtStartTime;
	private TextView txtEndTime;
	private TextView txtSongName;
	int n = 10000;

	Boolean isCancelled = false;
	Boolean isAlbum;
	private int mediaFileLengthInMilliseconds;

	private int selectedSongPosition;
	private Runnable notificationRunnable;
	private final Handler handler = new Handler();

	private String albumName;
	private String songName;
	private String imageUrl;
	public int songIndex;
	Random generator = new Random();

	// For DwonaLoad Notification.
	Notification notification;
	NotificationManager notificationManager;
	CharSequence contentText;
	Context context;

	// Display Notification,
	private long time;
	private int icon;
	CharSequence tickerText;
	PendingIntent contentIntent;
	CharSequence contentTitle;
	int HELLO_ID = 1;
	String fname;
	PlaySongAsy play;
	private String TAG = getClass().getSimpleName();
	private MediaPlayer mp = null;
	boolean isPlaying = false;
	ImageLoader imageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.utilsn = new Utilities();
		setContentView(R.layout.activity_playsong);
		imageLoader = new ImageLoader(PlaySongActivity.this);
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		intializaView();
		context = getApplicationContext();
		imageUrl = getIntent().getExtras().getString("imageUrl");
		albumName = getIntent().getExtras().getString("albumName");
		songName = getIntent().getExtras().getString("songName");
		songIndex = getIntent().getExtras().getInt("songIndex");
		isAlbum = getIntent().getExtras().getBoolean("isalbum");

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setImageonImageView(imgAlbumImage, imageUrl);
				txtSongName.setText(songName);

			}
		});

		/*
		 * play=(PlaySongAsy) new
		 * PlaySongAsy(DisplaySongActivity.songList.get(songIndex)
		 * .getAlbumSongsLink().replace(" ", "%20").trim()).execute();
		 */

	}

	class PlaySongAsy extends AsyncTask<String, Void, Boolean> {

		String baseURL;

		public PlaySongAsy(String baseURL) {
			this.baseURL = baseURL;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = ProgressDialog.show(PlaySongActivity.this,
					"    Buffering...", "please wait..", false);
			progressDialog.setCancelable(false);

		}

		@Override
		protected Boolean doInBackground(String... urls) {

			// play(baseURL);
			new Thread() {
				@Override
				public void run() {
					play(baseURL);
				}
			}.start();
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			// progressDialog.dismiss();
		}
	}

	private void play(String baseURL) {
		Uri myUri = Uri.parse(baseURL);
		try {
			if (mp == null) {
				this.mp = new MediaPlayer();
			} else {
				mp.stop();
				mp.reset();
			}
			mp.setDataSource(this, myUri); // Go to Initialized state

			mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.setOnPreparedListener(this);
			mp.setOnBufferingUpdateListener(this);
			mp.setOnErrorListener(this);

			mp.prepareAsync();

			// mp.setVolume(5.F, 5.F);

			Log.d(TAG, "LoadClip Done");
		} catch (Throwable t) {
			Log.d(TAG, t.toString());
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// mp.stop();

		isCancelled = true;

		// PlaySongActivity.this.finish();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		Log.d(TAG, "Stream is prepared");
		mp.start();
		isPlaying = true;
		btnPlayPause.setImageResource(R.drawable.pause);
	}

	private void pause() {
		mp.pause();
		isPlaying = false;
	}

	private void stop() {
		mp.stop();
		isPlaying = false;
		btnPlayPause.setImageResource(R.drawable.pause);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mp != null) {
			stop();
			mp.setOnBufferingUpdateListener(null);

		}

	}

	public void onCompletion(MediaPlayer mp) {
		stop();
	}

	public boolean onError(MediaPlayer mp, int what, int extra) {
		StringBuilder sb = new StringBuilder();
		sb.append("Media Player Error: ");
		switch (what) {
		case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
			sb.append("Not Valid for Progressive Playback");
			break;
		case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
			sb.append("Server Died");
			break;
		case MediaPlayer.MEDIA_ERROR_UNKNOWN:
			sb.append("Unknown");
			break;
		default:
			sb.append(" Non standard (");
			sb.append(what);
			sb.append(")");
		}
		sb.append(" (" + what + ") ");
		sb.append(extra);
		Log.e(TAG, sb.toString());
		return true;
	}

	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		if ((mp != null) && (mp.isPlaying())) {
			progressDialog.dismiss();

			primarySeekBarProgressUpdater();
		}
		if (isCancelled) {

		}
		Log.d(TAG, "PlayerService onBufferingUpdate : " + percent + "%");
		// bufPercent=percent;
	}

	private void intializaView() {
		// TODO Auto-generated method stub
		btnBack = (ImageView) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(onClickListener);

		btnMore = (ImageView) findViewById(R.id.btnHome);
		btnMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext(),
						more_activity.class);
				startActivity(intent);
			}
		});

		progressSongDuration = (SeekBar) findViewById(R.id.progbarSong);
		progressSongDuration.setMax(99); // It means 100% .0-99
		progressSongDuration.setOnTouchListener(this);

		imgAlbumImage = (ImageView) findViewById(R.id.imgAlbum);

		imgDownLoad = (ImageView) findViewById(R.id.imgDownload);
		imgDownLoad.setOnClickListener(onClickListener);

		imgShare = (ImageView) findViewById(R.id.imgShare);
		imgShare.setOnClickListener(onClickListener);

		btnNext = (ImageView) findViewById(R.id.btnNext);
		btnNext.setOnClickListener(onClickListener);

		btnPlayPause = (ImageView) findViewById(R.id.btnPlayPause);
		btnPlayPause.setOnClickListener(onClickListener);

		btnPrevious = (ImageView) findViewById(R.id.btnPrev);
		btnPrevious.setOnClickListener(onClickListener);

		txtStartTime = (TextView) findViewById(R.id.txtStartTime);
		txtEndTime = (TextView) findViewById(R.id.txtEndTime);

		txtSongName = (TextView) findViewById(R.id.txtSongName);
		txtSongName.setTextColor(Color.WHITE);
		txtSongName.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);

	}

	private void setImageonImageView(ImageView imgAlbumImage, String imageUrl) {
		try {
			imageLoader.DisplayImage(imageUrl, imgAlbumImage);

		} catch (Exception ex) {
			System.out.println("Error in load Image-->" + ex.toString());
		}
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btnBack) {
				finish();
			} else if (v.getId() == R.id.btnHome) {

			} else if (v.getId() == R.id.btnNext) {
				if (mp != null) {
					if (isInternetAvailable()) {
						stop();
						if (songIndex == DisplaySongActivity.songList.size() - 1) {
							songIndex = 0;
						} else {
							songIndex++;
						}
						play = (PlaySongAsy) new PlaySongAsy(
								DisplaySongActivity.songList.get(songIndex)
										.getAlbumSongsLink()
										.replace(" ", "%20").trim()).execute();
					} else {
						Toast.makeText(getApplicationContext(),
								"Internet service not available",
								Toast.LENGTH_LONG).show();
					}
				} else {
					if (songIndex == DisplaySongActivity.songList.size() - 1) {
						songIndex = 0;
					} else {
						songIndex++;
					}
					play = (PlaySongAsy) new PlaySongAsy(
							DisplaySongActivity.songList.get(songIndex)
									.getAlbumSongsLink().replace(" ", "%20")
									.trim()).execute();
				}
			} else if (v.getId() == R.id.btnPrev) {
				System.out.println("Button Previous Click");
				if (mp != null) {
					if (isInternetAvailable()) {
						stop();
						if (songIndex == 0) {
							songIndex = DisplaySongActivity.songList.size() - 1;
						} else {
							songIndex--;
						}
						play = (PlaySongAsy) new PlaySongAsy(
								DisplaySongActivity.songList.get(songIndex)
										.getAlbumSongsLink()
										.replace(" ", "%20").trim()).execute();

					} else {
						Toast.makeText(getApplicationContext(),
								"Internet service not available",
								Toast.LENGTH_LONG).show();
					}
				} else {
					if (songIndex == 0) {
						songIndex = DisplaySongActivity.songList.size() - 1;
					} else {
						songIndex--;
					}
					play = (PlaySongAsy) new PlaySongAsy(
							DisplaySongActivity.songList.get(songIndex)
									.getAlbumSongsLink().replace(" ", "%20")
									.trim()).execute();
				}
			} else if (v.getId() == R.id.btnPlayPause) {
				System.out.println("Button play Click");
				if (mp != null) {
					System.out.println("Button play Click");
					if (isPlaying) {
						isPlaying = false;
						pause();
						btnPlayPause.setImageResource(R.drawable.playicon);
					} else {
						isPlaying = true;
						mp.start();
						btnPlayPause.setImageResource(R.drawable.pause);
					}

				} else {
					play = (PlaySongAsy) new PlaySongAsy(
							DisplaySongActivity.songList.get(songIndex)
									.getAlbumSongsLink().replace(" ", "%20")
									.trim()).execute();
				}
			} else if (v.getId() == R.id.imgDownload) {
				System.out.println("Button Download Click");
				startDownload();

			} else if (v.getId() == R.id.imgShare) {
				System.out.println("Button share Click");
				// TODO Auto-generated method stub
				{
					Intent intent = new Intent(Intent.ACTION_SEND);
					intent.setType("text/plain");
					intent.putExtra(
							Intent.EXTRA_TEXT,
							"Tones "
									+ DisplaySongActivity.songList.get(
											songIndex).getAlbumSongsName()
									+ " from "
									+ albumName
									+ "\n"
									+ "Get All Type of Ringtones \n"
									+ "Download this App Click to Below link "
									+ "http://play.google.com/store/apps/details?id=com.freemusicappz.allinonemusic");
					startActivity(Intent.createChooser(intent, "Share via"));

					//
					// Intent playSongIntent = new Intent(
					// PlaySongActivity.this, MainActivity.class);
					// // playSongIntent.putExtra("imageUrl", imageUrl);
					// // playSongIntent.putExtra("albumName", albumName);
					// // playSongIntent.putExtra("songIndex", childPosition);
					// // playSongIntent.putExtra("songName",
					// // albumSongList.getAlbumSongsName());
					//
					// startActivity(playSongIntent);
				}
			}
			songName = DisplaySongActivity.songList.get(songIndex)
					.getAlbumSongsName();

			txtSongName.setText(songName);
		}

	};

	public boolean isInternetAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null)
			return (cm.getActiveNetworkInfo().isConnected() && cm
					.getActiveNetworkInfo().isAvailable());
		else {
			return false;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		if (v.getId() == R.id.progbarSong) {
			/**
			 * Seekbar onTouch event handler. Method which seeks MediaPlayer to
			 * seekBar primary progress position
			 */
			if (mp != null) {
				if (mp.isPlaying()) {
					SeekBar sb = (SeekBar) v;
					int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100)
							* sb.getProgress();
					mp.seekTo(playPositionInMillisecconds);
				}
			}
		}
		return false;
	}

	private void primarySeekBarProgressUpdater() {

		if (mp != null) {
			long l1 = mp.getDuration();
			long l2 = mp.getCurrentPosition();
			txtEndTime.setText(utilsn.milliSecondsToTimer(l1));
			txtStartTime.setText(utilsn.milliSecondsToTimer(l2));

			mediaFileLengthInMilliseconds = mp.getDuration();
			// SeekBar.setProgress((int)(100.0F * (this.mps.getCurrentPosition()
			// /
			// this.mediaFileLengthInMilliseconds)));
			int temp123 = mp.getCurrentPosition();

			float temp11 = ((temp123 * 100.0F) / mediaFileLengthInMilliseconds);

			int temp = (int) (temp11);
			progressSongDuration.setProgress(temp);
			if (mp.isPlaying()) {
				Runnable local12 = new Runnable() {
					public void run() {
						primarySeekBarProgressUpdater();
					}
				};
				this.handler.postDelayed(local12, 1000L);
			}
		}
	}

	private void startDownload() {

		new AlertDialog.Builder(this)
				.setIcon(R.drawable.download)
				.setTitle("Download File")
				.setMessage("Are you sure you want to Download this File?")
				.setPositiveButton("Download",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

								new Thread() {
									@Override
									public void run() {
										// TODO Auto-generated method stub

										super.run();

										DownloadFileAsync localDownloadFileAsync = new DownloadFileAsync(getApplicationContext());
										final String[] arrayOfString = new String[3];
										arrayOfString[0] = DisplaySongActivity.songList
												.get(songIndex)
												.getAlbumSongsLink();
										arrayOfString[1] = DisplaySongActivity.songList
												.get(songIndex)
												.getAlbumSongsName();
										if(isAlbum)
										{
										arrayOfString[2]="True";
										}
										else
										{
											arrayOfString[2]="False";	
										}

										localDownloadFileAsync
												.execute(arrayOfString);
										
										
										
//											Intent i = new Intent(PlaySongActivity.this,
//												DownloadsList.class);
//												
//												i.putExtra("url",DisplaySongActivity.songList.get(songIndex).getAlbumSongsLink());
//												i.putExtra("name",DisplaySongActivity.songList.get(songIndex).getAlbumSongsName());
//												if(isAlbum){
//												i.putExtra("album",true);
//												}else
//												{
//													i.putExtra("album",false);
//												}
//												i.putExtra("isfrom",true);
//
//												startActivity(i);
												
										

									}
								}.start();

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).show();

	}

	public void downloadNotification() {
		this.notificationManager = ((NotificationManager) getSystemService("notification"));
		this.icon = 2130837512;
		this.tickerText = "Downloading...";
		this.time = System.currentTimeMillis();
		this.notification = new Notification(R.drawable.ic_launcher,
				this.tickerText, this.time);
		Notification localNotification = this.notification;
		localNotification.flags = (0x18 | localNotification.flags);
		this.context = getApplicationContext();
		this.contentTitle = ("Downloading " + songName);
		this.contentText = "0% complete";
		Uri localUri = Uri.parse("file:///sdcard/Ringtones Downloaded/"
				+ songName);
		Intent localIntent = new Intent("android.intent.action.VIEW");
		localIntent.setDataAndType(localUri, "audio/mp3");
		this.contentIntent = PendingIntent.getActivity(this.context, 0,
				localIntent, 0);
		this.notification.setLatestEventInfo(this.context, this.contentTitle,
				this.contentText, this.contentIntent);
		this.notificationManager.notify(this.HELLO_ID, this.notification);
	}

	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();

	}

}

package com.muza.musicmania;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ProgressBar;

import com.muza.musicmania.DownloadInfo1.DownloadState;

/**
 * Simulate downloading a file, by increasing the progress of the FileInfo from 0 to size - 1.
 */
//public class FileDownloadTask extends AsyncTask<Void, Integer, Void> {
//  private static final String    TAG = FileDownloadTask.class.getSimpleName();
//  final DownloadInfo             mInfo;
//  
//  public FileDownloadTask(DownloadInfo info) {
//    mInfo = info;
//  }
//
//  @Override
//  protected void onProgressUpdate(Integer... values) {
//    mInfo.setProgress(values[0]);
//    ProgressBar bar = mInfo.getProgressBar();
//    if(bar != null) {
//      bar.setProgress(mInfo.getProgress());
//      bar.invalidate();
//    }
//  }
//
//  @Override
//  protected Void doInBackground(Void... params) {
//    Log.d(TAG, "Starting download for " + mInfo.getFilename());
//    mInfo.setDownloadState(DownloadState.DOWNLOADING);
//    for (int i = 0; i <= mInfo.getFileSize(); ++i) {
//      try {
//        Thread.sleep(16);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//      publishProgress(i);
//    }
//    mInfo.setDownloadState(DownloadState.COMPLETE);
//    return null;
//  }
//
//  @Override
//  protected void onPostExecute(Void result) {
//    mInfo.setDownloadState(DownloadState.COMPLETE);
//  }
//
//  @Override
//  protected void onPreExecute() {
//    mInfo.setDownloadState(DownloadState.DOWNLOADING);
//  }
//
//}
class FileDownloadTask extends AsyncTask<String, Integer, String> {
	File localFile2;
	String[] temp = new String[5];
	DownloadInfo1   mInfo=null;
	CharSequence contentText;

  public FileDownloadTask(DownloadInfo1 info) {
  mInfo = info;
}


	@Override
	protected String doInBackground(String... arg0) {

		this.temp=arg0;
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD)
		{
			new Thread()
			{
				public void run() 
				{
					String Temp = temp[1];

					File localFile1 = new File("/mnt/sdcard/New Ringtones Download");
					if (!localFile1.exists())
						localFile1.mkdirs();
					localFile2 = new File(localFile1, Temp);
					if (localFile2.exists()) {
						localFile2.delete();
					}

					try {

						String decoded = temp[0].replace(" ", "%20");

						URL localURL = new URL(decoded);
						URLConnection localURLConnection = localURL.openConnection();
						localURLConnection.connect();
					
						int i = localURLConnection.getContentLength();
						Log.d("ANDRO_ASYNC", "Lenght of file: " + i);
						BufferedInputStream localBufferedInputStream = new BufferedInputStream(
								localURL.openStream());
						FileOutputStream localFileOutputStream = new FileOutputStream(
								localFile2);
						byte[] arrayOfByte = new byte[1024];
						long l = 0L;
						while (true) {
							int j = localBufferedInputStream.read(arrayOfByte);
							if (j == -1) {
								localFileOutputStream.flush();
								localFileOutputStream.close();
								localBufferedInputStream.close();
								break;
							}
							l += j;
							String[] arrayOfString = new String[1];

							int t = (int) (100L * l / i);
							;
							arrayOfString[0] = "" + t;
							publishProgress(Integer.parseInt( arrayOfString[0]));
							localFileOutputStream.write(arrayOfByte, 0, j);
						}
					} catch (Exception localException) {
						Log.i("Ankit", localException.toString());

					
				}
				

				};
			}.start();

		}
		else
		{
			
			String Temp = temp[1];

			File localFile1 = new File("/mnt/sdcard/New Ringtones Download");
			if (!localFile1.exists())
				localFile1.mkdirs();
			localFile2 = new File(localFile1, Temp);
			if (localFile2.exists()) {
				localFile2.delete();
			}

			try {

				String decoded = temp[0].replace(" ", "%20");

				URL localURL = new URL(decoded);
				URLConnection localURLConnection = localURL.openConnection();
				localURLConnection.connect();
			
				int i = localURLConnection.getContentLength();
				Log.d("ANDRO_ASYNC", "Lenght of file: " + i);
				BufferedInputStream localBufferedInputStream = new BufferedInputStream(
						localURL.openStream());
				FileOutputStream localFileOutputStream = new FileOutputStream(
						localFile2);
				byte[] arrayOfByte = new byte[1024];
				long l = 0L;
				while (true) {
					int j = localBufferedInputStream.read(arrayOfByte);
					if (j == -1) {
						localFileOutputStream.flush();
						localFileOutputStream.close();
						localBufferedInputStream.close();
						break;
					}
					l += j;
					String[] arrayOfString = new String[1];

					int t = (int) (100L * l / i);
					;
					arrayOfString[0] = "" + t;
					publishProgress(Integer.parseInt( arrayOfString[0]));
					localFileOutputStream.write(arrayOfByte, 0, j);
				}
			} catch (Exception localException) {
				Log.i("Ankit", localException.toString());

			
		}
			
		}
		
		
		
							// TODO Auto-generated method stub
		
	
		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

	//	String temp = localFile2.toString();

//		MediaScannerConnection.scanFile(context,
//				new String[] { localFile2.toString() }, null,
//				new MediaScannerConnection.OnScanCompletedListener() {
//
//					@Override
//					public void onScanCompleted(String arg0, Uri arg1) {
//
//					}
//				});

	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	    mInfo.setDownloadState(DownloadState.DOWNLOADING);


	}
  @Override
  protected void onProgressUpdate(Integer... values) {
    mInfo.setProgress( values[0]);
    ProgressBar bar = mInfo.getProgressBar();
    if(bar != null) {
      bar.setProgress(mInfo.getProgress());
      bar.invalidate();
    }
  }


//	@Override
//	protected void onProgressUpdate(String... values) {
//		// TODO Auto-generated method stub
//		// Log.d("ANDRO_ASYNC", values[1]);
//		contentText = (Integer.parseInt(values[0]) + "% complete");
//
//		super.onProgressUpdate(values);
//	}
}


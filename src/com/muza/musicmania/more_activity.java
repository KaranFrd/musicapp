package com.muza.musicmania;



import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.muza.music.R;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class more_activity extends Activity {

	
	ImageButton btnShareApp, btnRateThisApp, btnGetMoreApps;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.more_activity);
		
		AdView adView = (AdView)this.findViewById(R.id.adView);
	    adView.loadAd(new AdRequest());
		
		btnShareApp=(ImageButton) findViewById(R.id.btnShareApp);
		btnRateThisApp = (ImageButton) findViewById(R.id.btnRateThisApp);
		btnGetMoreApps = (ImageButton) findViewById(R.id.btnGetMoreApps);
		
		btnShareApp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/plain");

				intent.putExtra(
						Intent.EXTRA_TEXT,
						"Download All Type of Ringtones App on Google Play.\n"
								+ "http://play.google.com/store/apps/details?id=com.musicworldapps.allringtones");
				startActivity(Intent.createChooser(intent, "Share App via:"));
			}
		});
		
		btnRateThisApp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("http://play.google.com/store/apps/details?id=com.musicworldapps.allringtones"));
				startActivity(intent);
			}
		});
		
		btnGetMoreApps.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri
						.parse("https://play.google.com/store/search?q=musicworldapps&c=apps"));
				startActivity(intent);
			}
		});
	}

}

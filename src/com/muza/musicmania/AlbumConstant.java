/**
 * 
 */
package com.muza.musicmania;

/**
 * @author janakz
 *
 */
public class AlbumConstant 
{
	public static String KEY_ALBUM_ID="id";
	public static String KEY_ALBUM_NAME="name";
	public static String KEY_ALBUM_IMAGE_URL="image";
	public static String KEY_ALBUM_ISACTIVE="is_active";
	public static String KEY_ALBUM_CREATED_ON="created_on";
	public static String KEY_ALBUM_UPDATE_ON="updated_on";
	
	public static String KEY_ALBUM_TRACKS="tracks";
	
	
	
}

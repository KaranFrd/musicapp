package com.muza.musicmania;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.muza.music.R;

public class Downloadsongcat extends Activity {

	Button btntone,btnsong;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_downloadsongcat);
		btntone=(Button)findViewById(R.id.btnTone1);
		btnsong=(Button)findViewById(R.id.btnSonglist);
		btnsong.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(Downloadsongcat.this,
						DownloadedSongList.class);
				i.putExtra("isalbum", true);

				startActivity(i);

				
				
			}
		});
		
		
		btntone.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// TODO Auto-generated method stub
				Intent i = new Intent(Downloadsongcat.this,
						DownloadedSongList.class);
				i.putExtra("isalbum", false);

				startActivity(i);

				
				
			}
		});		
		
		
		
	}



}

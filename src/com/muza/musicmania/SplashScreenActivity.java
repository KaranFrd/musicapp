package com.muza.musicmania;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import com.muza.music.R;

public class SplashScreenActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
   //     this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    //    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
      //          WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.activity_splash_screen);
        
       
        if (isInternetAvailable()) {
    		
        	 new Handler().postDelayed(new Runnable()
             {
                 @Override
                 public void run()
                 {
                     final Intent mainIntent = new Intent(SplashScreenActivity.this, homepage.class);
                     SplashScreenActivity.this.startActivity(mainIntent);
                     SplashScreenActivity.this.finish();

                 }
             }, 2000);
             
        	
    	} else {

    		AlertDialog.Builder builder = new AlertDialog.Builder(
    				SplashScreenActivity.this);
    		builder.setTitle("Internet service not available")
    				.setPositiveButton(
    						"OK",
    						new android.content.DialogInterface.OnClickListener() {

    							@Override
    							public void onClick(DialogInterface dialog,
    									int which) {
    								// TODO Auto-generated method stub
    								SplashScreenActivity.this.finish();
    							}
    						});
    		builder.create().show();

    	}
        
    }

    public boolean isInternetAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null) {
			return (cm.getActiveNetworkInfo().isConnected() && cm
					.getActiveNetworkInfo().isAvailable());
		} else {
			return false;
		}
	}   

}


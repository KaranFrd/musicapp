package com.muza.musicmania;

import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import com.muza.music.R;

public class AndroidTabAndListView extends TabActivity {
	// TabSpec Names
	private static final String SERVER1 = "SERVER1";
	private static final String SERVER2 = "SERVER2";
	private static final String SERVER3 = "SERVER3";
	private static final String SERVER4 = "SERVER4";

//	EditText editText;
//	Button btngo;
ImageView imgMusic,imgMusic1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);     //  Fixed Portrait orientation
        final TabHost tabHost = getTabHost();
        
        // Inbox Tab
        TabSpec inboxSpec = tabHost.newTabSpec(SERVER1);
        
    	imgMusic = (ImageView) findViewById(R.id.imgMusic);
		imgMusic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				
				Intent i = new Intent(AndroidTabAndListView.this,
						DownloadedSongList.class);
				i.putExtra("isalbum", true);

				startActivity(i);
			}
		});
		imgMusic1=(ImageView)findViewById(R.id.imgMusic1);

		imgMusic1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				
		Intent playSongIntent = new Intent(AndroidTabAndListView .this, MainActivity.class);
//				playSongIntent.putExtra("imageUrl", imageUrl);
//				playSongIntent.putExtra("albumName", albumName);
//				playSongIntent.putExtra("songIndex", childPosition);
//				playSongIntent.putExtra("songName",
//						albumSongList.getAlbumSongsName());

				startActivity(playSongIntent);
			}
		});
        
      //  editText=(EditText)findViewById(R.id.editText1);
	//	btngo = (Button) findViewById(R.id.btnGo);
		
//		btngo.setOnClickListener(new OnClickListener() {
//		
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Cons.listAlbums.clear();
//				Cons.songname=editText.getText().toString();
//                Log.i("***Selected Tab", "Im currently in tab with index::" + tabHost.getCurrentTab());
//
//				switch (tabHost.getCurrentTab()) {
//				case 0:
//					new InboxActivity().updtae();
//
//					break;
//				case 1:
//	            	OutboxActivity.updatedata();
//
//					break;
//				case 2:
//	            	ProfileActivity.updatedata();
//
//					break;
//
//				default:
//					break;
//				}
//				InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
//				.getSystemService(Context.INPUT_METHOD_SERVICE);
//		inputManager.hideSoftInputFromWindow(getCurrentFocus()
//				.getWindowToken(),
//				InputMethodManager.HIDE_NOT_ALWAYS);
//
//			}
//		});
//		
//		
		
        // Tab Icon
        inboxSpec.setIndicator(SERVER1);
        Intent inboxIntent = new Intent(this, InboxActivity.class);
        // Tab Content
        inboxSpec.setContent(inboxIntent);
        
        // Outbox Tab
        TabSpec outboxSpec = tabHost.newTabSpec(SERVER2);
        outboxSpec.setIndicator(SERVER2);
        Intent outboxIntent = new Intent(this, ProfileActivity.class);
        outboxSpec.setContent(outboxIntent);
        
        // Profile Tab
        TabSpec profileSpec = tabHost.newTabSpec(SERVER4);
        profileSpec.setIndicator(SERVER4);
        Intent profileIntent = new Intent(this, OutboxActivity.class);
        profileSpec.setContent(profileIntent);
        
        

//        TabSpec chinaserver = tabHost.newTabSpec(SERVER4);
//        chinaserver.setIndicator(SERVER4);
//        Intent chinaserverintent = new Intent(this, Chinaserver.class);
//        chinaserver.setContent(chinaserverintent);

        
        TabSpec profileSpec1 = tabHost.newTabSpec(SERVER3);
        profileSpec1.setIndicator(SERVER3);
        Intent chinaintent = new Intent(this, Chinaserver.class);
        profileSpec1.setContent(chinaintent);
        
        
        
        
        

        // Adding all TabSpec to TabHost
        tabHost.addTab(inboxSpec); // Adding Inbox tab

        tabHost.addTab(outboxSpec); // Adding Outbox tab
        tabHost.addTab(profileSpec1); // Adding Profile tab

        tabHost.addTab(profileSpec); // Adding Profile tab

        
        tabHost.setOnTabChangedListener(new OnTabChangeListener() {

            @Override
            public void onTabChanged(String arg0) {         
            	

            	switch (tabHost.getCurrentTab()) {
				case 0:
					
					new InboxActivity().updtae();

					break;
				case 1:
	            	OutboxActivity.updatedata();

					break;
				case 2:
	            	ProfileActivity.updatedata();

					break;
				case 3:
	        //    	Chinaserver.updatedata();

					break;
				default:
					break;
				}
            	

                Log.i("***Selected Tab", "Im currently in tab with index::" + tabHost.getCurrentTab());
            }       
        });  
        
        
    
        OnTouchListener focusHandler = new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                view.requestFocusFromTouch();
                return false;
            }
        };
   //     editText.setOnTouchListener(focusHandler); //For each EditText with this issue

    }
}
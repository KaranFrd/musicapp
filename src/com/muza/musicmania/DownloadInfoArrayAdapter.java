package com.muza.musicmania;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.muza.music.R;

@SuppressLint("NewApi") public class DownloadInfoArrayAdapter extends ArrayAdapter<DownloadInfo1> {
  // Simple class to make it so that we don't have to call findViewById frequently
  private static class ViewHolder {
    TextView textView;
    ProgressBar progressBar;
    //Button button;
    DownloadInfo1 info;
    TextView txtdownloadprogress;
    
  }
  
  
  private static final String TAG = DownloadInfoArrayAdapter.class.getSimpleName();
  Context con;
  public DownloadInfoArrayAdapter(Context context, int textViewResourceId,
      List<DownloadInfo1> objects) {
    super(context, textViewResourceId, objects);
    con=context;
  }
  
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View row = convertView;
    final DownloadInfo1 info = getItem(position);
    // We need to set the convertView's progressBar to null.

   ViewHolder holder = null;
    final Button btn;
    if(null == row) {
      LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      row = inflater.inflate(R.layout.file_download_row, parent, false);
      
      holder = new ViewHolder();
      holder.textView = (TextView) row.findViewById(R.id.downloadFileName);
      holder.progressBar = (ProgressBar) row.findViewById(R.id.downloadProgressBar);
      //holder.button = (Button)row.findViewById(R.id.downloadButton);
      holder.info = info;
      holder.txtdownloadprogress=(TextView) row.findViewById(R.id.downloadprogress);
      row.setTag(holder);
    } else {
      holder = (ViewHolder) row.getTag();
      
      holder.info.setProgressBar(null);
      holder.info = info;
      holder.info.setProgressBar(holder.progressBar);
      holder.info.settextview(holder.txtdownloadprogress);
      
    }
    Resources res = con.getResources();
    holder.progressBar.setProgressDrawable(res.getDrawable(R.drawable.greenprogressbar));

   // btn=holder.button;
    holder.txtdownloadprogress.setText(""+info.getdownloadsize());

    holder.textView.setText(info.getFilename());
    holder.progressBar.setProgress(info.getProgress());
    holder.progressBar.setMax(info.getFileSize());
    info.setProgressBar(holder.progressBar);
    info.settextview(holder.txtdownloadprogress);
//	 if(info.isPause)
//	 {
//		 btn.setText("Resume");
//		 
//	 }
//	 else
//	 {
//		 btn.setText("Pause");
//	 }

    
//    holder.button.setEnabled(info.getDownloadState() == DownloadState.NOT_STARTED);
   // final Button button = holder.button;
   // holder.button.setOnClickListener(new OnClickListener() {
//      @Override
//      public void onClick(View v) 
//      {
//    	 info.isPause=!info.isPause;
//    	 if(info.isPause)
//    	 {
//    		 btn.setText("Resume");
//    		 
//    	 }
//    	 else
//    	 {
//    		 btn.setText("Pause");
//    	 }
//      }
//    });
    
    
    //TODO: When reusing a view, invalidate the current progressBar.
    
    return row;
  }

}

/**
 * 
 */
package com.muza.musicmania;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muza.allringtones.lazy.FileCache;
import com.muza.music.R;

/**
 * @author janakz
 * 
 */
public class DisplaySongActivity extends Activity {

	private ExpandableListView expandableListView;
	private String TEMP_STRING = new FileCache().temp;
	private List<AlbumModel> albumModelsParent;
	private HashMap<AlbumModel, List<AlbumSongList>> albumSongListChild;
	private List<AlbumSongList> albumSongLists;
	private AlbumModel albumModel;
	private AlbumSongList albumSongList;
	public TextView txtTop;
	private ExpandableListAdapter expListAdapter;

	ImageView imgMusic1;
	public static List<AlbumSongList> songList;
	private ImageView imgMusic;
	public Boolean isalbum;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent1 = getIntent();
		isalbum = intent1.getBooleanExtra("isalbum", true);
		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		// WindowManager.LayoutParams.FLAG_FULLSCREEN);
		if (!isTaskRoot()) {
			final Intent intent = getIntent();
			final String intentAction = intent.getAction();
			if (intent.hasCategory(Intent.CATEGORY_LAUNCHER)
					&& intentAction != null
					&& intentAction.equals(Intent.ACTION_MAIN)) {
				// Log.w(LOG_TAG,
				// "Main Activity is not the root.  Finishing Main Activity instead of launching.");
				finish();
				return;
			}
		}
		setContentView(R.layout.activity_displaysong);
		txtTop = (TextView) findViewById(R.id.txtAlbumName);
		if (isalbum) {

			TEMP_STRING = new FileCache().temp1;

			txtTop.setText("Albums");
		} else

		{
			TEMP_STRING = new FileCache().temp;
			txtTop.setText("Tones Category");

		}

		// NotificationManager nm = (NotificationManager)
		// getSystemService(Context.NOTIFICATION_SERVICE);
		// Intent intent = new Intent(Intent.ACTION_VIEW);
		// intent.setData(Uri
		// .parse("https://play.google.com/store/apps/details?id=com.musicworldapps.moodscanner"));
		// PendingIntent contentIntent = PendingIntent.getActivity(
		// DisplaySongActivity.this, 143, intent,
		// PendingIntent.FLAG_UPDATE_CURRENT);
		// NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
		// DisplaySongActivity.this).setSmallIcon(R.drawable.ic_launcher)
		// .setContentTitle("Mood Thumb Scanner in Android")
		// .setContentText("Get Mood Scanner in your Android Device.")
		// .setTicker("Get Mood Scanner in Android")
		// .setContentIntent(contentIntent).setAutoCancel(true);
		// nm.notify(143, mBuilder.build());

		// AdView adView = (AdView)this.findViewById(R.id.adView);
		// adView.loadAd(new AdRequest());
		imgMusic = (ImageView) findViewById(R.id.imgMusic);
		imgMusic.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent i = new Intent(DisplaySongActivity.this,
						DownloadedSongList.class);
				i.putExtra("isalbum", isalbum);

				startActivity(i);
			}
		});

		imgMusic1 = (ImageView) findViewById(R.id.imgMusic1);

		imgMusic1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent playSongIntent = new Intent(DisplaySongActivity.this,
						MainActivity.class);
				// playSongIntent.putExtra("imageUrl", imageUrl);
				// playSongIntent.putExtra("albumName", albumName);
				// playSongIntent.putExtra("songIndex", childPosition);
				// playSongIntent.putExtra("songName",
				// albumSongList.getAlbumSongsName());

				startActivity(playSongIntent);
				
				
				
//					Intent i = new Intent(DisplaySongActivity.this,
//						DownloadsList.class);
//						i.putExtra("isfrom",true);
//						startActivity(i);
						
			}
		});
		expandableListView = (ExpandableListView) findViewById(R.id.exp_listview);

		expandableListView.setBackgroundColor(Color.TRANSPARENT);
		expandableListView.setCacheColorHint(Color.TRANSPARENT);

		if (isInternetAvailable()) {
			getAlbumDetails();
		} else {

			AlertDialog.Builder builder = new AlertDialog.Builder(
					DisplaySongActivity.this);
			builder.setTitle("Internet service not available")
					.setPositiveButton(
							"OK",
							new android.content.DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									DisplaySongActivity.this.finish();
								}
							});
			builder.create().show();

		}

		expandableListView.setOnChildClickListener(new OnChildClickListener() {
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				AlbumSongList albumSongList = (AlbumSongList) expListAdapter
						.getChild(groupPosition, childPosition);
				AlbumModel albumModel = albumModelsParent.get(groupPosition);
				String imageUrl = albumModel.getAlbumImageUrl();
				String albumName = albumModel.getAlbumName();
				songList = albumSongListChild.get(albumModel);
				if (isInternetAvailable()) {
					Intent playSongIntent = new Intent(
							DisplaySongActivity.this, PlaySongActivity.class);
					playSongIntent.putExtra("imageUrl", imageUrl);
					playSongIntent.putExtra("albumName", albumName);
					playSongIntent.putExtra("songIndex", childPosition);
					playSongIntent.putExtra("songName",
							albumSongList.getAlbumSongsName());

					playSongIntent.putExtra("isalbum", isalbum);

					startActivity(playSongIntent);
				} else

				{
					AlertDialog.Builder builder = new AlertDialog.Builder(
							DisplaySongActivity.this);
					builder.setTitle("Internet service not available")
							.setPositiveButton(
									"OK",
									new android.content.DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// TODO Auto-generated method stub
										}
									});
					builder.create().show();

				}
				return true;
			}
		});

	}

	// @Override
	// public void onBackPressed() {
	// // TODO Auto-generated method stub
	// AlertDialog.Builder builder = new AlertDialog.Builder(
	// DisplaySongActivity.this);
	// builder.setIcon(R.drawable.ic_launcher);
	// builder.setTitle("Songs Collection");
	// builder.setMessage("Latest new songs Download free!")
	// .setPositiveButton("Exit", new OnClickListener() {
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// finish();
	// }
	// })
	// .setNegativeButton("Install",
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setData(Uri
	// .parse("http://play.google.com/store/apps/details?id=com.musicworldapps.newmp3songs"));
	// startActivity(intent);
	// dialog.cancel();
	// }
	// })
	// .setNeutralButton("Rate Us!",
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	//
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setData(Uri
	// .parse("http://play.google.com/store/apps/details?id=com.musicworldapps.allringtones"));
	// startActivity(intent);
	// dialog.cancel();
	// }
	// }).show();
	//
	// }

	public void getAlbumDetails() {
		class DownloadWebPageTask extends AsyncTask<String, Void, Boolean> {
			ProgressDialog progressDialog;

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				if (isalbum) {
					progressDialog = ProgressDialog.show(
							DisplaySongActivity.this, "Load Albums",
							"please wait..");
				} else {
					progressDialog = ProgressDialog.show(
							DisplaySongActivity.this, "Load Ringtones",
							"please wait..");

				}
			}

			@Override
			protected Boolean doInBackground(String... urls) {
				try {
					HttpParams httpParams = new BasicHttpParams();
					HttpConnectionParams
							.setConnectionTimeout(httpParams, 30000);
					HttpConnectionParams.setSoTimeout(httpParams, 30000);
					HttpParams p = new BasicHttpParams();
					// Instantiate an HttpClient
					HttpClient httpclient = new DefaultHttpClient(p);
					HttpPost httppost = new HttpPost(TEMP_STRING);
					ResponseHandler<String> responseHandler = new BasicResponseHandler();
					String responseBody = httpclient.execute(httppost,
							responseHandler);
					JSONObject finalJson = new JSONObject(responseBody);

					if (finalJson.getString("status").equals("1")) {
						JSONArray arrayAlbums = finalJson.getJSONArray("data");
						// Log.i("SplashScreenActivity", "JSON Response: "+
						// arrayAlbums.toString());
						if (arrayAlbums.length() > 0) {
							albumModelsParent = new ArrayList<AlbumModel>();
							albumSongListChild = new HashMap<AlbumModel, List<AlbumSongList>>();
							for (int i = 0; i < arrayAlbums.length(); i++) {

								JSONObject albumJsonObject = arrayAlbums
										.getJSONObject(i);
								albumModel = new AlbumModel();
								albumSongList = new AlbumSongList();

								albumModel.setAlbumId(albumJsonObject
										.getString(AlbumConstant.KEY_ALBUM_ID));
								albumModel
										.setAlbumName(albumJsonObject
												.getString(AlbumConstant.KEY_ALBUM_NAME));
								albumModel
										.setAlbumImageUrl(albumJsonObject
												.getString(AlbumConstant.KEY_ALBUM_IMAGE_URL));
								albumModel
										.setAlbumActive(albumJsonObject
												.getString(AlbumConstant.KEY_ALBUM_ISACTIVE));
								albumModel
										.setAlbumCreatedOn(albumJsonObject
												.getString(AlbumConstant.KEY_ALBUM_CREATED_ON));
								albumModel
										.setAlbumUpdateOn(albumJsonObject
												.getString(AlbumConstant.KEY_ALBUM_UPDATE_ON));
								albumModelsParent.add(albumModel);

								JSONArray songsArray = albumJsonObject
										.getJSONArray(AlbumConstant.KEY_ALBUM_TRACKS);
								albumSongLists = new ArrayList<AlbumSongList>();
								for (int j = 0; j < songsArray.length(); j++) {
									albumSongList = new AlbumSongList();
									albumSongList.setAlbumSongsLink(songsArray
											.getString(j));
									albumSongList
											.setAlbumSongsName(getSongsName(songsArray
													.getString(j)));
									albumSongLists.add(albumSongList);
								}

								albumSongListChild.put(albumModel,
										albumSongLists);
							}
							return true;
						} else {
							return false;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
				return false;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				if (result) {
					progressDialog.dismiss();
					runOnUiThread(new Runnable() {
						public void run() {
							expListAdapter = new ExpandableListAdapter(
									DisplaySongActivity.this,
									albumModelsParent, albumSongListChild);
							expandableListView.setAdapter(expListAdapter);
						}
					});

				} else

				{
					Toast.makeText(getApplicationContext(),
							"Please check your internet conncetion!", 10)
							.show();
					progressDialog.dismiss();
				}
			}

			private String getSongsName(String url) {
				String songName = null;
				String[] spilt = url.split("/");
				for (int j = 0; j < spilt.length; j++) {
					if (j == spilt.length - 1) {
						songName = spilt[j];
					}
				}
				return songName;
			}
		}

		if (isInternetAvailable()) {
			new DownloadWebPageTask().execute();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					DisplaySongActivity.this);
			builder.setTitle("Internet Service not available")
					.setPositiveButton("Ok", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});

		}
	}

	public boolean isInternetAvailable() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null) {
			return (cm.getActiveNetworkInfo().isConnected() && cm
					.getActiveNetworkInfo().isAvailable());
		} else {
			return false;
		}
	}

	protected void onStart() {

		super.onStart();

	}

	protected void onRestart() {
		super.onRestart();

	}

	protected void onResume() {
		super.onResume();

	}

	protected void onPause() {
		super.onPause();

	}

	protected void onStop() {
		super.onStop();

	}

	protected void onDestroy()

	{

		super.onDestroy();

	}
	/*
	 * public void onBackPressed() { Log.d("CDA", "onBackPressed Called");
	 * Intent setIntent = new Intent(Intent.ACTION_MAIN);
	 * setIntent.addCategory(Intent.CATEGORY_HOME);
	 * setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	 * startActivity(setIntent); }
	 */
}

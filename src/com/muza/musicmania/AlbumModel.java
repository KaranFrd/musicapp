/**
 * 
 */
package com.muza.musicmania;

/**
 * @author janakz
 *
 */
public class AlbumModel 
{
	private String albumId;
	private String albumName;
	private String albumImageUrl;
	private String albumActive;
	private String albumCreatedOn;
	private String albumUpdateOn;
	/**
	 * @return the albumId
	 */
	public String getAlbumId() {
		return albumId;
	}
	/**
	 * @param albumId the albumId to set
	 */
	public void setAlbumId(String albumId) {
		this.albumId = albumId;
	}
	/**
	 * @return the albumName
	 */
	public String getAlbumName() {
		return albumName;
	}
	/**
	 * @param albumName the albumName to set
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	/**
	 * @return the albumImageUrl
	 */
	public String getAlbumImageUrl() {
		return albumImageUrl;
	}
	/**
	 * @param albumImageUrl the albumImageUrl to set
	 */
	public void setAlbumImageUrl(String albumImageUrl) {
		this.albumImageUrl = albumImageUrl;
	}
	/**
	 * @return the albumActive
	 */
	public String getAlbumActive() {
		return albumActive;
	}
	/**
	 * @param albumActive the albumActive to set
	 */
	public void setAlbumActive(String albumActive) {
		this.albumActive = albumActive;
	}
	/**
	 * @return the albumCreatedOn
	 */
	public String getAlbumCreatedOn() {
		return albumCreatedOn;
	}
	/**
	 * @param albumCreatedOn the albumCreatedOn to set
	 */
	public void setAlbumCreatedOn(String albumCreatedOn) {
		this.albumCreatedOn = albumCreatedOn;
	}
	/**
	 * @return the albumUpdateOn
	 */
	public String getAlbumUpdateOn() {
		return albumUpdateOn;
	}
	/**
	 * @param albumUpdateOn the albumUpdateOn to set
	 */
	public void setAlbumUpdateOn(String albumUpdateOn) {
		this.albumUpdateOn = albumUpdateOn;
	}
}

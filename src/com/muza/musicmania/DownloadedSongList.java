package com.muza.musicmania;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.muza.music.R;

public class DownloadedSongList extends Activity {
	String ringurl;
	Boolean isalbum;
	TextView txtsongsheading;
	String MEDIA_PATH = new String("/mnt/sdcard/Songs Downloaded/");

	TextView txtsongname;
	ListView listdownloadsongs;

	class FileExtensionFilter implements FilenameFilter {
		FileExtensionFilter() {
		}

		public boolean accept(File paramFile, String paramString) {
			return (paramString.endsWith(".mp3"))
					|| (paramString.endsWith(".MP3"));
		}
	}

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.songlist);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		isalbum = getIntent().getExtras().getBoolean("isalbum");
		txtsongsheading = (TextView) findViewById(R.id.txtsongsheading);

		if (isalbum) {
			MEDIA_PATH = new String("/mnt/sdcard/Songs Downloaded");
			txtsongsheading.setText("Mp3 Songs");
		} else {
			MEDIA_PATH = new String("/mnt/sdcard/Ringtones Downloaded");
			txtsongsheading.setText("Ringtones");

		}
		AdView adView = (AdView) this.findViewById(R.id.adView);
		adView.loadAd(new AdRequest());
		// Initiate a generic request to load it with an ad

		txtsongname = (TextView) findViewById(R.id.txtsongsheading);
		listdownloadsongs = (ListView) findViewById(R.id.listdownload);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, mp3select());

		listdownloadsongs.setAdapter(adapter);
		// listdownloadsongs.setBackgroundColor(color.transparent);

		listdownloadsongs.setCacheColorHint(Color.TRANSPARENT);
		listdownloadsongs.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {

				String[] arrayOfString = { "Play", "Default Ringtone",
						"Assign To Contact", "Default Notification",
						"Delete Song" };

				AlertDialog.Builder localBuilder = new AlertDialog.Builder(
						DownloadedSongList.this);

				localBuilder.setIcon(R.drawable.ic_launcher);
				localBuilder.setTitle("Settings").setItems(arrayOfString,
						new DialogInterface.OnClickListener() {
							String str = listdownloadsongs.getItemAtPosition(
									position).toString();

							public void onClick(
									DialogInterface paramDialogInterface,
									int paramInt) {
								// if (paramInt>0)
								// {
								// SetRingtone(str+".mp3",);
								// return;
								// }
								Uri localUri;
								if (!isalbum) {
									localUri = Uri
											.parse("file:///sdcard/Ringtones Downloaded/"
													+ str + ".mp3");

								} else {
									localUri = Uri
											.parse("file:///sdcard/Songs Downloaded/"
													+ str + ".mp3");

								}
								switch (paramInt) {
								case 0:

									Intent localIntent = new Intent(
											"android.intent.action.VIEW");
									localIntent.setDataAndType(localUri,
											"audio/mp3");
									DownloadedSongList.this
											.startActivity(localIntent);
									break;
								case 1:
									SetRingtone(str + ".mp3", 1);
									break;
								case 2:
									ringurl = str + ".mp3";
									SetRingtoneparticluarcontact(str + ".mp3",
											1);

									break;
								case 3:
									SetRingtone(str + ".mp3", 2);

									break;
								case 4:
									SetRingtone(str + ".mp3", 4);
								case 5: {
									try {
										File localFile1;

										if (isalbum) {
											localFile1 = new File(
													"/mnt/sdcard/Songs Downloaded");
										} else {
											localFile1 = new File(
													"/mnt/sdcard/Ringtones Downloaded");
										}
										if (!localFile1.exists())
											localFile1.mkdirs();
										File newSoundFile = new File(
												localFile1, str + ".mp3");

										DeleteRecursive(newSoundFile);
										boolean deleted = localFile1.delete();
										ArrayAdapter<String> adapter = new ArrayAdapter<String>(
												DownloadedSongList.this,
												android.R.layout.simple_list_item_1,
												mp3select());
										listdownloadsongs.setAdapter(adapter);

									} catch (Exception e) {
										// TODO Auto-ee catch block
										e.printStackTrace();
									}

									break;
								}

								default:
									break;
								}

							}
						});
				localBuilder.create();
				localBuilder.show();
			}
		});

	}

	private void SetRingtoneparticluarcontact(final String temp,
			final int tonenumber) {

		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		File localFile1;

		if (isalbum) {
			localFile1 = new File("/mnt/sdcard/Songs Downloaded");
		} else {
			localFile1 = new File("/mnt/sdcard/Ringtones Downloaded");
		}
		if (!localFile1.exists())
			localFile1.mkdirs();
		File newSoundFile = new File(localFile1, ringurl);
		switch (requestCode) {
		case (1):
			if (resultCode == Activity.RESULT_OK) {

				try {
					Uri contactData = data.getData();
					String contactId = contactData.getLastPathSegment();
					String[] PROJECTION = new String[] {
							ContactsContract.Contacts._ID,
							ContactsContract.Contacts.DISPLAY_NAME,
							ContactsContract.Contacts.HAS_PHONE_NUMBER, };
					Cursor localCursor = getContentResolver().query(
							contactData, PROJECTION, null, null, null);
					localCursor.moveToFirst();
					// --> use moveToFirst instead of this:
					// localCursor.move(Integer.valueOf(contactId)); /*CONTACT
					// ID NUMBER*/

					String contactID = localCursor.getString(localCursor
							.getColumnIndexOrThrow("_id"));
					String contactDisplayName = localCursor
							.getString(localCursor
									.getColumnIndexOrThrow("display_name"));

					Uri localUri = Uri.withAppendedPath(
							ContactsContract.Contacts.CONTENT_URI, contactID);
					localCursor.close();
					ContentResolver mCr = getApplicationContext()
							.getContentResolver();
					ContentValues values = new ContentValues();
					values.put(MediaStore.MediaColumns.DATA,
							newSoundFile.getAbsolutePath());
					values.put(MediaStore.MediaColumns.TITLE, "my Notification");
					values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
					values.put(MediaStore.MediaColumns.SIZE,
							newSoundFile.length());
					values.put(MediaStore.Audio.Media.ARTIST, R.string.app_name);
					values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
					values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
					values.put(MediaStore.Audio.Media.IS_ALARM, true);
					values.put(MediaStore.Audio.Media.IS_MUSIC, true);

					Uri uri = MediaStore.Audio.Media
							.getContentUriForPath(newSoundFile
									.getAbsolutePath());
					Uri newUri = mCr.insert(uri, values);

					values.put("custom_ringtone", newUri.toString());
					int j = getContentResolver().update(localUri, values, null,
							null);

					Toast.makeText(this,
							"Ringtone assigned to: " + contactDisplayName,
							Toast.LENGTH_LONG).show();

				} catch (Exception ex) {
					Log.d("Ankit", "" + ex);
				}
			}
			break;
		}

		// if (c.moveToFirst()) {
		// String name =
		// c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		// // TODO Fetch other Contact details as you want to use
		//
		// }

	}

	void DeleteRecursive(File dir) {
		Log.d("DeleteRecursive", "DELETEPREVIOUS TOP" + dir.getPath());
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				File temp = new File(dir, children[i]);
				if (temp.isDirectory()) {
					Log.d("DeleteRecursive", "Recursive Call" + temp.getPath());
					DeleteRecursive(temp);
				} else {
					Log.d("DeleteRecursive", "Delete File" + temp.getPath());
					boolean b = temp.delete();
					if (b == false) {
						Log.d("DeleteRecursive", "DELETE FAIL");
					}
				}
			}

		}
		dir.delete();
	}

	private void SetRingtone(final String temp, final int tonenumber) {
		new Thread() {
			public void run() {
				try {

					// ContentResolver mCr = getApplicationContext()
					// .getContentResolver();
					//
					// File localFile1;
					//
					// if(isalbum)
					// {
					// localFile1 = new File("/mnt/sdcard/New Songs");
					// }
					// else
					// {
					// localFile1 = new
					// File("/mnt/sdcard/Ringtones Downloaded");
					// }
					// if (!localFile1.exists())
					// localFile1.mkdirs();
					// File newSoundFile = new File(localFile1, temp);
					File newSoundFiledir = new File("/sdcard/media/ringtone");
					File newSoundFile1 = new File(
							"/sdcard/Ringtones Downloaded", temp);

					if (isalbum) {
						newSoundFile1 = new File("/sdcard/Songs Downloaded",
								temp);
					} else {
						newSoundFile1 = new File(
								"/sdcard/Ringtones Downloaded", temp);
					}

					String tempnew = temp + "" + new java.util.Date();

					String result = tempnew.replaceAll("[-+.^:,]", "");
					File newSoundFile = new File("/sdcard/media/ringtone"
							+ result + ".mp3");

					Uri mUri = Uri
							.parse("android.resource://com.your.package/R.raw.your_resource_id");
					ContentResolver mCr = getApplicationContext()
							.getContentResolver();
					AssetFileDescriptor soundFile;
					try {
						soundFile = mCr.openAssetFileDescriptor(mUri, "r");
					} catch (FileNotFoundException e) {
						soundFile = null;
					}

					try {
						byte[] readData = new byte[1024];
						FileInputStream fis = new FileInputStream(newSoundFile1);

						if (!newSoundFile.getParentFile().exists()) {
							newSoundFiledir.getParentFile().mkdir();
						}

						if (!newSoundFiledir.exists())
							newSoundFiledir.mkdir();

						if (!newSoundFile.exists()) {
							newSoundFile.createNewFile();
						} else {
							boolean deleted = newSoundFile.delete();

							if (deleted) {
								newSoundFile.createNewFile();
							}

						}

						FileOutputStream fos = new FileOutputStream(
								newSoundFile);

						int i = fis.read(readData);

						while (i != -1) {
							fos.write(readData, 0, i);
							i = fis.read(readData);
						}

						fos.close();
					} catch (IOException io) {

						io.printStackTrace();
					}

					if (newSoundFile.exists())
						try {

							ContentValues values = new ContentValues();
							values.put(MediaStore.MediaColumns.DATA,
									newSoundFile.getAbsolutePath());
							values.put(MediaStore.MediaColumns.TITLE, temp);
							values.put(MediaStore.MediaColumns.MIME_TYPE,
									"audio/mp3");
							values.put(MediaStore.MediaColumns.SIZE,
									newSoundFile.length());
							values.put(MediaStore.Audio.Media.ARTIST,
									R.string.app_name);
							values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
							values.put(MediaStore.Audio.Media.IS_NOTIFICATION,
									true);
							values.put(MediaStore.Audio.Media.IS_ALARM, true);
							values.put(MediaStore.Audio.Media.IS_MUSIC, false);

							Uri uri = MediaStore.Audio.Media
									.getContentUriForPath(newSoundFile
											.getAbsolutePath());
							Uri newUri = mCr.insert(uri, values);

							try {
								RingtoneManager.setActualDefaultRingtoneUri(
										getApplicationContext(),
										RingtoneManager.TYPE_RINGTONE, newUri);
							} catch (Throwable t) {
								Log.d("Ankit", "catch exception");
							}

							// // ContentValues values = new ContentValues();
							// // values.put(MediaStore.MediaColumns.DATA,
							// // newSoundFile.getAbsolutePath());
							// // values.put(MediaStore.MediaColumns.TITLE,
							// // temp);
							// // values.put(MediaStore.MediaColumns.MIME_TYPE,
							// // "audio/mp3");
							// // values.put(MediaStore.MediaColumns.SIZE,
							// // newSoundFile.length());
							// // values.put(MediaStore.Audio.Media.ARTIST,
							// // R.string.app_name);
							// // values.put(MediaStore.Audio.Media.IS_RINGTONE,
							// true);
							//
							// Uri uri =
							// MediaStore.Audio.Media.getContentUriForPath(newSoundFile.getAbsolutePath());
							//
							//
							//
							//
							//
							// try {
							//
							//
							// RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),
							// tonenumber, uri);
							// } catch (Throwable t) {
							//
							// Log.d("Ankit", "catch exception" + t);
							//
							// }

						} catch (Exception localException3) {
							Log.d("Ankit", "catch exception" + localException3);

						}
				} catch (Exception localException1) {

					Log.d("Ankit", "catch exception" + localException1);

				}
				try {
					return;
				} catch (Exception localException2) {
				}
			}
		}.start();
	}

	public ArrayList<String> mp3select() {
		File localFile1 = new File(this.MEDIA_PATH);
		ArrayList localArrayList = new ArrayList();

		if (localFile1.isDirectory()) {
			if (localFile1.listFiles(new FileExtensionFilter()).length > 0) {
				File[] arrayOfFile = localFile1
						.listFiles(new FileExtensionFilter());

				int i = arrayOfFile.length - 1;

				for (int j = i;; j--) {

					if (j == -1)
						return localArrayList;

					File localFile2 = arrayOfFile[j];
					localArrayList.add(localFile2.getName().substring(0,
							-4 + localFile2.getName().length()));

				}
			}
			if (isalbum) {
				AlertDialog localAlertDialog = new AlertDialog.Builder(this)
						.create();
				localAlertDialog.setTitle("No One Downloaded Ringtones");
				localAlertDialog
						.setMessage("No Tones Available Please Download Tones.");
				localAlertDialog.setButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								//
								arg0.dismiss();
							}
						});

				localAlertDialog.show();
			} else {

				AlertDialog localAlertDialog = new AlertDialog.Builder(this)
						.create();
				localAlertDialog.setTitle("No One Downloaded Songs");
				localAlertDialog
						.setMessage("No Songs Available Please Download Songs.");
				localAlertDialog.setButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								//
								arg0.dismiss();
							}
						});

				localAlertDialog.show();

			}

		} else {
			if (isalbum) {
				AlertDialog localAlertDialog = new AlertDialog.Builder(this)
						.create();
				localAlertDialog.setTitle("No One Downloaded Songs");
				localAlertDialog
						.setMessage("No Songs Available Please Download Songs.");
				localAlertDialog.setButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								//
								arg0.dismiss();
							}
						});

				localAlertDialog.show();

			} else {

				AlertDialog localAlertDialog = new AlertDialog.Builder(this)
						.create();
				localAlertDialog.setTitle("No One Downloaded Ringtones");
				localAlertDialog
						.setMessage("No Tones Available Please Download Tones.");
				localAlertDialog.setButton("Ok",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								//
								arg0.dismiss();
							}
						});

				localAlertDialog.show();

			}

		}

		return localArrayList;

	}

}

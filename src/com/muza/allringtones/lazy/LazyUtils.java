 package com.muza.allringtones.lazy;

import java.io.InputStream;
import java.io.OutputStream;

public class LazyUtils
{
    // private static final String EXT_STORAGE_PATH_PREFIX = "/Android/data/";
    // private static final String EXT_STORAGE_CACHE_PATH_SUFFIX = "/cache/";
    // public static final Object[] DATA_LOCK = new Object[0];

    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size = 1024;
        try
        {
            byte[] bytes = new byte[buffer_size];
            for (;;)
            {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                {
                    break;
                }
                os.write(bytes, 0, count);
            }
        } catch (Exception ex)
        {
        }
    }

    // public static Bitmap getRoundedCornerBitmap(final Bitmap bitmap, final
    // float round)
    // {
    // final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
    // bitmap.getHeight(), Config.ARGB_8888);
    // final Canvas canvas = new Canvas(output);
    //
    // final Paint paint = new Paint();
    // final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    // final RectF rectF = new RectF(rect);
    //
    // paint.setAntiAlias(true);
    // canvas.drawARGB(0, 0, 0, 0);
    // canvas.drawRoundRect(rectF, round, round, paint);
    //
    // paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
    // canvas.drawBitmap(bitmap, rect, rect, paint);
    //
    // return output;
    // }

    // public static File getExternalCacheDirAllApiLevels(final String
    // packageName)
    // {
    // return LazyUtils.getExternalDirAllApiLevels(packageName,
    // EXT_STORAGE_CACHE_PATH_SUFFIX);
    // }
    //
    // private static File getExternalDirAllApiLevels(final String packageName,
    // final String suffixType)
    // {
    // final File dir = new File(Environment.getExternalStorageDirectory() +
    // EXT_STORAGE_PATH_PREFIX + packageName
    // + suffixType);
    // synchronized (LazyUtils.DATA_LOCK)
    // {
    // try
    // {
    // dir.mkdirs();
    // dir.createNewFile();
    // } catch (final IOException e)
    // {
    // Log.e(ApplicationConstants.LOG_TAG, "Error creating file", e);
    // }
    // }
    // return dir;
    // }
}
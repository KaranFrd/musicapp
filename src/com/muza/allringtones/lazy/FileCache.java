package com.muza.allringtones.lazy;

import java.io.File;

import android.content.Context;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
public class FileCache
{

    private File cacheDir;
    public String temp="dfsdfsdf";
    public String temp1="dfsdfsdf";

    public String gettemp()
 {
  
  return temp;
 }
    public String gettemp1()
    {
     
     return temp1;
    }
    
    public FileCache()
    {
     
     
     try {
         temp=  decrypt("1517158456556","8B09E63002359C9649D987A5A4A0619B90728D04D74BC97650590ADA76C6CE93AF09DA94549751D83AAB6098A33D70F0BBE1652A06DC9411B0186C54A77DC7CF0BADD396D09048972B1EA8636CE50F39");
         temp1=  decrypt("1517158456556","8B09E63002359C9649D987A5A4A0619B5C7C02B8FCBCCD1BDA32E508308AB1B638E3C916165CBC132B95FF6F88A6D16FFE9BE261232310FC83D0F169E1441C2A4374B278DCB0A6B52F6C8C871EABE80D");


 } catch (Exception e) {
  // TODO: handle exception
 }
    }
    

    public FileCache(Context context)
    {
     
     
        // Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
        {
            this.cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "LazyList");
            // this.cacheDir =
            // LazyUtils.getExternalCacheDirAllApiLevels("com.shore.swiggle");
        } else
        {
            this.cacheDir = context.getCacheDir();
        }
        if (!this.cacheDir.exists())
        {
            this.cacheDir.mkdirs();
        }
    }

    public File getFile(String url)
    {
        // I identify images by hashcode. Not a perfect solution, good for the
        // demo.
        String filename = String.valueOf(url.hashCode());
        // Another possible solution (thanks to grantland)
        // String filename = URLEncoder.encode(url);
        File f = new File(this.cacheDir, filename);
        return f;

    }

    public void clear()
    {
        File[] files = this.cacheDir.listFiles();
        if (files == null)
        {
            return;
        }
        for (File f : files)
        {
            f.delete();
        }
    }

public static String decrypt(String seed, String encrypted) throws Exception {
        byte[] rawKey = getRawKey(seed.getBytes());
        byte[] enc = toByte(encrypted);
        byte[] result = decrypt(rawKey, enc);
        return new String(result);
}

private static byte[] getRawKey(byte[] seed) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
     //   SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(seed);
    kgen.init(128, sr); // 192 and 256 bits may not be available
    SecretKey skey = kgen.generateKey();
    byte[] raw = skey.getEncoded();
    return raw;
}



private static byte[] decrypt(byte[] raw, byte[] encrypted) throws Exception {
    SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
    cipher.init(Cipher.DECRYPT_MODE, skeySpec);
    byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
}

public static String toHex(String txt) {
        return toHex(txt.getBytes());
}
public static String fromHex(String hex) {
        return new String(toByte(hex));
}

public static byte[] toByte(String hexString) {
        int len = hexString.length()/2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++)
                result[i] = Integer.valueOf(hexString.substring(2*i, 2*i+2), 16).byteValue();
        return result;
}

public static String toHex(byte[] buf) {
        if (buf == null)
                return "";
        StringBuffer result = new StringBuffer(2*buf.length);
        for (int i = 0; i < buf.length; i++) {
                appendHex(result, buf[i]);
        }
        return result.toString();
}
private final static String HEX = "0123456789ABCDEF";
private static void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b>>4)&0x0f)).append(HEX.charAt(b&0x0f));
}
}
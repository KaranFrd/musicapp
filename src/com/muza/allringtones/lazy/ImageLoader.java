package com.muza.allringtones.lazy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import com.muza.music.R;


public class ImageLoader
{

    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Handler handler = new Handler();// handler to display images in UI thread

    public ImageLoader(Context context)
    {
        this.fileCache = new FileCache(context);
        this.executorService = Executors.newFixedThreadPool(5);
    }

    final int stub_id = R.drawable.ic_launcher;

    public void DisplayImage(String url, ImageView imageView)
    {
        this.imageViews.put(imageView, url);
        Bitmap bitmap = this.memoryCache.get(url);
        if (bitmap != null)
        {
            imageView.setImageBitmap(bitmap);

        } else
        {
            this.queuePhoto(url, imageView);
            imageView.setImageResource(this.stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView)
    {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        this.executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String url)
    {
        File f = this.fileCache.getFile(url);

        // from SD cache
        Bitmap b = this.decodeFile(f);
        if (b != null)
        {
            return b;
        }

        // from web
        try
        {
            Bitmap bitmap = null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is = conn.getInputStream();
            OutputStream os = new FileOutputStream(f);
            LazyUtils.CopyStream(is, os);
            os.close();
            conn.disconnect();
            bitmap = this.decodeFile(f);
            return bitmap;
        } catch (Throwable ex)
        {
            ex.printStackTrace();
            if (ex instanceof OutOfMemoryError)
            {
                this.memoryCache.clear();
            }
            return null;
        }
    }

    // decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f)
    {
       
        	try {
                //Decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(f),null,o);

                //The new size we want to scale to
                final int REQUIRED_SIZE=70;

                //Find the correct scale value. It should be the power of 2.
                int scale=1;
                while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
                    scale*=2;

                //Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize=scale;
                return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
            } catch (FileNotFoundException e) {}
        return null;
    }

    // Task for the queue
    private class PhotoToLoad
    {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i)
        {
            this.url = u;
            this.imageView = i;
        }
    }

    class PhotosLoader implements Runnable
    {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad)
        {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run()
        {
            try
            {
                if (ImageLoader.this.imageViewReused(this.photoToLoad))
                {
                    return;
                }
                Bitmap bmp = ImageLoader.this.getBitmap(this.photoToLoad.url);
                ImageLoader.this.memoryCache.put(this.photoToLoad.url, bmp);
                if (ImageLoader.this.imageViewReused(this.photoToLoad))
                {
                    return;
                }
                BitmapDisplayer bd = new BitmapDisplayer(bmp, this.photoToLoad);
                ImageLoader.this.handler.post(bd);
            } catch (Throwable th)
            {
                th.printStackTrace();
            }
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad)
    {
        String tag = this.imageViews.get(photoToLoad.imageView);
        if ((tag == null) || !tag.equals(photoToLoad.url))
        {
            return true;
        }
        return false;
    }

    // Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p)
        {
            this.bitmap = b;
            this.photoToLoad = p;
        }

        @Override
        public void run()
        {
            if (ImageLoader.this.imageViewReused(this.photoToLoad))
            {
                return;
            }
            if (this.bitmap != null)
            {
                this.photoToLoad.imageView.setImageBitmap(this.bitmap);
            } else
            {
                this.photoToLoad.imageView.setImageResource(ImageLoader.this.stub_id);
            }
        }
    }

    public void clearCache()
    {
        this.memoryCache.clear();
        this.fileCache.clear();
    }

}
